#ifndef HEADER_EA05437544357327
#define HEADER_EA05437544357327


#include "MaximumCliqueBase.h"


template<class ColorSort>
struct McrSort : public ColorSort {
    using ColorSort::numberSort;
    typedef typename ColorSort::GraphType GraphType;
    typedef typename GraphType::VertexSet VertexSet;
    typedef typename ColorSort::NumberedSet NumberedSet;
    
    struct Vertex {
        int degree;
        int exDegree;
        int index;
    };
    
    using ColorSort::init;
    using ColorSort::assignVertexNumber;
        
    void initialSort(VertexSet& c, VertexSet& vertices, NumberedSet& color) {
        size_t n = vertices.size();
        if (n == 0)
            return;
        auto maxDegree = this->graph->degrees[0];
        std::vector<Vertex> r(n);
        
        for (size_t i = 0; i < n; ++i) {
            //r[i].index = vertices[i];
            r[i].index = i;
            r[i].degree = this->graph->degrees[r[i].index];
            r[i].exDegree = 0;
            maxDegree = std::max(maxDegree, r[i].degree);
        }
        
        // not sure if the following calculation is correct for ex-deg (not clearly specified in Tomita 2006)
        //  it is possible this should be done on every step of the following while loop, taking only
        //  the neighbourhood of the observed vertex into an account
        for (size_t i = 0; i < n; ++i) 
            for (size_t j = 0; j < n; ++j) 
                if (this->graph->adjacencyMatrix[i][j] == 1)
                    r[i].exDegree += r[j].degree;
                    
        // sort by degree (descending), stable mode (respect relative order of the vertices with the same degree)
        std::stable_sort(r.begin(), r.end(), [](const Vertex& a, const Vertex& b){return (a.degree > b.degree);});
                
        // index in vertices
        size_t vi = n-1;
        
        // locate vertices with min degree
        size_t rMinIndex = r.size()-1;
        while (rMinIndex > 0) {
            int minDeg = r.back().degree;
            rMinIndex = r.size()-1;
            // set of vertices "Rmin" is implemented as a subarray from index rMinIndex to the end of the set r
            while ((rMinIndex > 0) && (r[rMinIndex-1].degree == minDeg)) 
                --rMinIndex;
            if (rMinIndex < r.size()-1) {
                // sort by ex-deg (descending - max is first)
                std::stable_sort(r.begin()+rMinIndex, r.end(), [](const Vertex& a, const Vertex& b){return (a.exDegree > b.exDegree);});
            }
            // vertex with min ex-deg in rMin
            Vertex& p = r.back();
            vertices[vi] = p.index; ///!!!
            --vi;
            // decrease the degree of remaining vertices that are adjacent to p
            for (auto& v : r)
                if (this->graph->adjacencyMatrix[v.index][p.index] == 1)
                    --v.degree;
            r.pop_back();
            
            std::stable_sort(r.begin(), r.end(), [](const Vertex& a, const Vertex& b){return (a.degree > b.degree);});
        }
//      std::cout << "DEBUG: iterated to regular subgraph of degree " << r.front().degree << " and size " << vi << "\n";
        // all the vertices in r have the same degree (regular subgraph) → perform ordinary number sort (color sort) 
        
        VertexSet emptySet;
        c.reserve(r.size());
        for (size_t i = 0; i < r.size(); ++i)
            c.add(r[i].index);
        numberSort(emptySet, c, color, 0);          // why the ordinary sort here, there is nothing to sort, c is a clique; check this in Tomita 2008
        for (size_t i = 0; i < c.size(); ++i)
            vertices[i] = c[i];     ///!!!
        // number
        size_t mmax = r.size() + maxDegree - color.back();
        size_t m = color.back();
        color.resize(n); // first few colors remain as they are
        for (size_t i = r.size(); i < n; ++i) {
            if (i < mmax) {
                ++m;
                assignVertexNumber(vertices, color, i, vertices[i], m);
            } else {
                assignVertexNumber(vertices, color, i, vertices[i], maxDegree + 1);
            }
        }
        
       // for (size_t i = 0; i < n; ++i)
         //   std::cout << i << ": " << color[i].first << "," << color[i].second << "  ";
        
        c.clear();
    }
};
REGISTER_TEMPLATE_EXT_CLASS_NAME(McrSort, "MCR sort");




#endif // header guard 
