#include "Bbmc.h"


Bbmc::Bbmc() {
    algorithmName = "BBMC";
}


Bbmc::Bbmc(int n, const std::vector<std::vector<int> >& A, const std::vector<int>& degree, int sortingStyle) :
    Mcq(n, A, degree, sortingStyle)
{
    algorithmName = "BBMC";
    N.resize(n);
    invN.resize(n);
    V.resize(n);
    
    timers.set(timer_impl1, "1", timer_search);
    timers.set(timer_impl2, "2", timer_search);
    timers.set(timer_impl3, "2", timer_search);
}

void Bbmc::search() {
    timers.reset();
    ScopeTimer tm(timers[timer_total]);
    BitSet C(n);
    BitSet P(n);
    {
        ScopeTimer tm1(timers[timer_init]);
        countExpands = 0;
        
        for (int i = 0; i < n; i++) { 
            N[i].resize(n);
            invN[i].resize(n);
            V[i].init(i, degree[i]);
        }
        orderVertices();
        C.reset(0, n);
        P.set(0, n);
    }
    ScopeTimer tm2(timers[timer_search]);
    expand(C, 0, P, n);
}

void Bbmc::expand(BitSet& C, size_t Csize, BitSet& P, size_t Psize) {
    countExpands++;
    
    VertexList U(Psize);
    VertexList color(Psize);
    numberSort(P, Psize, U, color);
    for (int i = Psize-1; i >= 0; i--) {
        if (color[i] + Csize <= maxSize) return;
        BitSet newP = P;
        int v = U[i];
        C[v] = true; 
        ++Csize;
        newP &= N[v];
        Psize = newP.count();
        if (Psize == 0 && Csize > maxSize) 
            saveSolution(C);
        if (Psize > 0) 
            expand(C, Csize, newP, Psize);
        P[v] = false; 
        C[v] = false;
        --Csize;
    }
}

void Bbmc::numberSort(BitSet& P, size_t Psize, VertexList& U, VertexList& color) {
    ScopeTimer tm(timers[timer_color_sort]);
    BitSet copyP = P;
    int colorClass = 0;
    int i = 0;
    while (Psize != 0) {
        colorClass++;
        BitSet Q = copyP;
        size_t Qsize = Psize;
        while (Qsize != 0) {
            int v = Q.nextSetBit();
            copyP[v] = false;
            --Psize;
            //Q[v] = false; // this has been added to invN
            Q &= invN[v];
            Qsize = Q.count();
            U[i] = v;     
            color[i++] = colorClass;
        }
    }
}

void Bbmc::orderVertices() {
    ScopeTimer tm(timers[timer_initial_sort]);
    for (int i = 0; i < n; i++) {
        V[i].init(i, degree[i]);
        for (int j = 0; j < n; j++) 
            if (A[i][j] == 1) V[i].nebDeg = V[i].nebDeg + degree[j];
    }
    if (sortingStyle == sort_degree) // default sorting style
        std::sort(V.begin(), V.end());
    else if (sortingStyle == sort_minWidth) 
        minWidthOrder(V);
    else if (sortingStyle == sort_mcr) {
        std::sort(V.begin(), V.end(), 
            [](const ExtendedVertex& v1, const ExtendedVertex& v2) {
                return (v1.degree > v2.degree || (v1.degree == v2.degree && v1.nebDeg > v2.nebDeg) ||
                    (v1.degree == v2.degree && v1.nebDeg == v2.nebDeg && v1.index < v2.index));
            }
        );
    }
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            int u = V[i].index;
            int v = V[j].index;
            N[i][j] = (A[u][v] == 1);
            invN[i][j] = (A[u][v] == 0);
        }
        invN[i][i] = false;
    }
}


void Bbmc::saveSolution(const BitSet& C) {
    maxSize = C.count();
    solution.resize(maxSize);
    for (size_t i = 0, j = 0; i < C.size(); i++) 
        if (C[i]) 
            solution[j++] = V[i].index;
}

