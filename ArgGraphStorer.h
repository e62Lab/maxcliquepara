#ifndef ARG_GRAPH_STORER_H
#define ARG_GRAPH_STORER_H


/**
 * This file contains the class for loading ARG library graphs, as provided here: 
 *  <http://mivia.unisa.it/datasets/graph-database/arg-database/>
 * 
 * File format is as follows: 
 * 
 */
#include <GraphLabels.h>
#include <utility>
#include <vector>
#include <string>

#include <fstream>


class ArgGraphStorer {
    std::string error;
    std::vector<std::vector<char> > adjacencyMatrix;
    GraphLabels<unsigned> labels;

public:
    void setup(std::vector<std::vector<char> >&& adjacency, GraphLabels<unsigned>&& labels) {
        adjacencyMatrix = std::move(adjacency);
        this->labels = std::move(labels);
    }

    bool store(const char* fname);

    std::string getErrorMsg() const { return error; }

protected:
    /**
     * @brief File is composed of word records, this function writes a single record
     * @param infile
     * @return 
     */
    void writeWord(std::ofstream& outfile, uint16_t word) {
        //std::cout << "write " << word << " -> ";
        uint8_t a = (uint8_t)(word & 0xFF);
        uint8_t b = (uint8_t)(word >> 8);
        //std::cout << (int)a << " " << (int)b <<"\n";
        outfile.put(a);
        outfile.put(b);
    }
};

#endif // ARG_GRAPH_STORER_H