#include "PdbGraphLoader.h"
#include <fstream>
#include <regex>
#include <iostream>
#include <algorithm>
#include <cassert>


PdbGraphLoader::PdbGraphLoader() {

}

PdbGraphLoader::~PdbGraphLoader() {

}

bool PdbGraphLoader::load(const char* fname) {
    std::ifstream file(fname);
    error = "";

    if (!file) {
        error = "ifstream invalid - file "+std::string(fname)+" cannot be opened for reading";
        return false;
    }
    
    // read line by line, process individual lines 
    int lineNumber = 0;
    std::string line;
    try {
        // regex for parsing HETATM line
        std::regex ws_re("\\s+");
        // prepare to load atom ids as labels
        vertexLabels.clear();
        edgeLabels.clear();
        std::map<std::string, uint16_t> atomMap;
        uint16_t lastNumber = 0;
        // line by line reading happens inside the following loop
        while (file && error.empty()) {
            std::getline(file, line);
            ++lineNumber;
            
            // correctly formed file does not seem to contain empty lines; END marks end of file
            if (line.empty() || (line == "END"))
                break;

            std::istringstream lineStream(line);
            std::vector<std::string> lineSplit(
                ( std::istream_iterator<std::string>(lineStream)),
                  std::istream_iterator<std::string>() );
            
            if (DEBUG_LOADER) {
                std::cout << lineNumber << "\t" << lineSplit.size() << " ";
                if (lineSplit.size() > 0) 
                    std::cout << lineSplit[0];
                std::cout << "\n";
            }

            if (lineSplit.size() > 0) {
                // a line with a record (identifier of what kind of line it is) and some parameters
                auto& record = lineSplit[0];
                
                if (record == "HETATM") {
                    // HETAHM is used to count vertices and to assign labels
                    if (lineSplit.size() > 1) {
                        int num = std::stoi(lineSplit[1]);
                        firstVertexNum = std::min(num, firstVertexNum);
                        lastVertexNum = std::max(num, lastVertexNum);
                        auto atom = lineSplit[10];
                        auto iMap = atomMap.find(atom);
                        if (iMap == atomMap.end()) {
                            lastNumber++;
                            atomMap[atom] = lastNumber;
                        }
                        vertexLabels.push_back(atomMap[atom]);
                    } else {
                        error += "Error parsing line "+std::to_string(lineNumber)+": "+line;
                        break;
                    }
                }
                
                if (record == "CONECT") {
                    if (lineSplit.size() <= 2) {
                        error += "Error parsing line "+std::to_string(lineNumber)+": "+line;
                        break;
                    }
                    
                    // on the very first CONNECT, the matrix will not be prepared
                    if (successorMatrix.size() == 0) {
                        const int limit = 10000;
                        if ((lastVertexNum > limit) || (lastVertexNum < 1)) {
                            error == "Number of vertices is too large ("+std::to_string(lastVertexNum)+"), this library has a limit of "+std::to_string(limit);
                            break;
                        } else {
                            successorMatrix.resize(lastVertexNum+1);
                            initializeEdgeLabels(lastVertexNum+1);
                        }
                    }

                    unsigned int i = std::stoi(lineSplit[1]);
                    // expect at least 2 parameters, first is the origin node, all following are destination nodes
                    for (unsigned int j = 2; j < lineSplit.size(); ++j) {
                        int si = std::stoi(lineSplit[j]);
                        insertToMatrix(i, si);
                        insertToMatrix(si, i);                       
                    }
                }
            }
        } // end of reading lines        
        
        if (DEBUG_LOADER) {
            std::cout << "\n " << successorMatrix.size() << " vertices\n";
            if (successorMatrix.size() > 0)
            for (size_t i = 0; i < successorMatrix.size(); ++i) {
                std::cout << i << ": ";
                for (size_t  j = 0; j < successorMatrix[i].size(); ++j)
                    std::cout << successorMatrix[i][j] << " ";
                std::cout << ";\n";
            }
        }        
    } catch (std::invalid_argument& e) {
        error = "Invalid argument provided to stoi in line " + std::to_string(lineNumber)+ ": " + line;
    }
    
    if (lineNumber <= lastVertexNum) {
        error = "The number of lines read is too small: " + std::to_string(lineNumber);
        return false;
    } else 
        return true;
}

unsigned int PdbGraphLoader::getNumVertices() const {
    // the graph enumeration starts with 1
    return lastVertexNum - firstVertexNum;
}

// TODO: typo in func name
unsigned int PdbGraphLoader::getMaxVertiexIndex() const {
    return lastVertexNum;
}

unsigned int PdbGraphLoader::getNumEdges() const {
    unsigned int s = 0;
    for (size_t i = 0; i < successorMatrix.size(); ++i) {
        s += successorMatrix[i].size();
    }
    return s/2;
}

std::vector<std::vector<char> > PdbGraphLoader::getAdjacencyMatrix() const {
    std::vector<std::vector<char>> adjacency(successorMatrix.size());
    for (size_t i = 0; i < successorMatrix.size(); ++i) {
        adjacency[i].resize(successorMatrix.size(), false);
        for (size_t j = 0; j < successorMatrix[i].size(); ++j) {
            size_t i2 = successorMatrix[i][j];
            adjacency[i][i2] = true;
        }
    }
    return adjacency;
}

std::vector<int> PdbGraphLoader::getDegrees() const {
    std::vector<int> degrees(successorMatrix.size(), 0);
    for (size_t i = 0; i < successorMatrix.size(); ++i) {
        degrees[i] = successorMatrix[i].size();
    }
    return degrees;
}
 
void PdbGraphLoader::insertToMatrix(unsigned int i1, unsigned int i2) {
    assert(i1 < successorMatrix.size());
    assert(i2 < successorMatrix.size());

    int insertPos = 0;
    for (size_t i = 0; i < successorMatrix[i1].size(); ++i) {
        if (successorMatrix[i1][i] < i2) {
            insertPos = i+1;
        } else if (successorMatrix[i1][i] == i2) {
        insertPos = -1;
        break;
    } else
        break;
    }
    if (insertPos != -1) {
        successorMatrix[i1].insert(successorMatrix[i1].begin()+insertPos, i2);
        edgeLabels[i1][i2] = 1;
    }
}

