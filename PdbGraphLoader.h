#ifndef PDB_GRAPH_LOADER_H
#define PDB_GRAPH_LOADER_H


/**
 * 
 */


#include <utility>
#include <vector>
#include <string>
#include <GraphLoader.h>
#include <GraphLabels.h>


/**
 * @class PdbGraphLoader
 * @author Matjaž
 * @date 28/01/20
 * @file PdbGraphLoader.h
 * @brief 
 */
class PdbGraphLoader : public GraphLoader, public GraphLabels<uint16_t> {
public:
    bool DEBUG_LOADER = false;
    
protected:
    std::string error;
    int firstVertexNum = 0, lastVertexNum = 0;
    std::vector<std::vector<unsigned int>> successorMatrix;
    
public:
    PdbGraphLoader();
    ~PdbGraphLoader();
    
    /**
     * @brief Load a .pdb file (contains much more than just a greph)
     * @param fname
     * @return 
     */
    virtual bool load(const char* fname) override;
    unsigned int getNumVertices() const override;
    unsigned int getMaxVertiexIndex() const override;
    unsigned int getNumEdges() const override;
    std::vector<std::vector<char> > getAdjacencyMatrix() const override;
    std::vector<int> getDegrees() const override;
	const std::string& getError() const override {return error;}
    bool verticesAreMappedFrom1based() const override {return false;}
    GraphLabels<uint16_t> getLabelling() const              { return *this; }
    
protected:
    /**
     * Call this to insert a pair of vertices to the successor matrix
     * It contains a check if the vertices are already conencted
     * */
    void insertToMatrix(unsigned int i1, unsigned int i2);
};


#endif //PBD_GRAPH_LOADER_H
