#ifndef HEADER_72793BBBF335BF57
#define HEADER_72793BBBF335BF57


#include <random>
#include "MaximumCliqueBase.h"


// works only on vector sets
template<class ColorSort>
struct DegreeSort : ColorSort {
    using ColorSort::numberSort;
    typedef typename ColorSort::GraphType GraphType;
    typedef typename GraphType::VertexSet VertexSet;
    typedef typename ColorSort::NumberedSet NumberedSet;
    

    using ColorSort::init;
    using ColorSort::assignVertexNumber;
    using ColorSort::notEmpty;
    using ColorSort::topNumber;
    using ColorSort::topVertex;
    using ColorSort::popTop;
    

    void initialSort(VertexSet& c, VertexSet& vertices, NumberedSet& color) {
        typedef typename GraphType::VertexId VertexId;
        typedef std::pair<VertexId,VertexId> VerDeg;
        
        // sort by degree
        size_t n = vertices.size();
        size_t maxDegree = 0;
        std::vector<VerDeg> vertexAndDegree(n);
        for (size_t i = 0; i < n; ++i) {
            vertexAndDegree[i] = std::make_pair(i, this->graph->degrees[i]);
            maxDegree = std::max(maxDegree, (size_t)this->graph->degrees[i]);
        }
        // sort by degree in descending order
        std::stable_sort(vertexAndDegree.begin(), vertexAndDegree.end(), [](const VerDeg& a, const VerDeg& b){return (a.second > b.second);});
        
        // reorder vertices in graph
        std::vector<VertexId> vertexSet;
        vertexSet.reserve(n);
        for (const auto& vAd : vertexAndDegree)
            vertexSet.push_back(vAd.first);
        this->graph->orderVertices(vertexSet);
        
        // initial number sort
        color.resize(n);
        for (size_t i = 0; i < n; ++i) 
            assignVertexNumber(vertices, color, i, i, 1+std::min(i, maxDegree));
    }
};
REGISTER_TEMPLATE_EXT_CLASS_NAME(DegreeSort, "Degree sort");


template<class ColorSort>
struct RandDegreeSort : ColorSort {
    using ColorSort::numberSort;
    typedef typename ColorSort::GraphType GraphType;
    typedef typename GraphType::VertexSet VertexSet;
    typedef typename ColorSort::NumberedSet NumberedSet;

    using ColorSort::init;
    using ColorSort::assignVertexNumber;
    using ColorSort::notEmpty;
    using ColorSort::topNumber;
    using ColorSort::topVertex;
    using ColorSort::popTop;
    
    void initialSort(VertexSet& c, VertexSet& vertices, NumberedSet& color) {
        typedef typename GraphType::VertexId VertexId;
        typedef std::pair<VertexId,double> VerDeg;
        
        // init random generator
        auto rndEng = std::mt19937(std::random_device{}());
        std::uniform_real_distribution<double> rndDist(0.1, 0.9);  //(min, max)
        
        // sort by degree
        size_t n = vertices.size();
        size_t maxDegree = 0;
        std::vector<VerDeg> vertexAndDegree(n);
        for (size_t i = 0; i < n; ++i) {
            vertexAndDegree[i] = std::make_pair(i, (double)this->graph->degrees[i]+rndDist(rndEng));
            maxDegree = std::max(maxDegree, (size_t)this->graph->degrees[i]);
        }
        
        /*
        double sum = 0, mn = 0, mx = 0;
        for (size_t cnt = 0; cnt < 100000; ++cnt) {
            double r = rndDist(rndEng);
            sum += r;
            if (cnt == 0) mn = mx = r;
            else {
                mn = std::min(mn, r);
                mx = std::max(mx, r);
            }
        }
        std::cout << "Random: [" << mn << " - " << mx << "], mean = " << sum/100000 << "\n";
        */
        
        // sort by degree in descending order
        std::stable_sort(vertexAndDegree.begin(), vertexAndDegree.end(), [](const VerDeg& a, const VerDeg& b){return (a.second > b.second);});
        
        // reorder vertices in graph
        std::vector<VertexId> vertexSet;
        vertexSet.reserve(n);
        for (const auto& vAd : vertexAndDegree)
            vertexSet.push_back(vAd.first);
        this->graph->orderVertices(vertexSet);
        
        // initial number sort
        color.resize(n);
        for (size_t i = 0; i < n; ++i) 
            assignVertexNumber(vertices, color, i, i, 1+std::min(i, maxDegree));
    }
};
REGISTER_TEMPLATE_EXT_CLASS_NAME(RandDegreeSort, "Randomized Degree sort");

#endif // header guard 
