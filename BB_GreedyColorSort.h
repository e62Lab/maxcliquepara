#ifndef BB_GREEDYCOLORSORT_H_INCLUDED
#define BB_GREEDYCOLORSORT_H_INCLUDED


#include "BitstringSet.h"
#include "MaximumCliqueBase.h"
#include "SteadyGreedyColorSort.h" // SteadyVectorSet is reused here


template<class Graph>
class BBGreedyColorSort {
public:
    typedef typename Graph::VertexSet VertexSet;
    typedef typename VertexSet::VertexId VertexId;
    typedef SteadyVectorSet<VertexId> NumberedSet;
    typedef Graph GraphType;
    
protected:
    std::vector<VertexSet> colorSet;
    Graph* graph;
    Timers* timers;
    VertexSet Ubb, Qbb;
    
public:
    BBGreedyColorSort() : graph(nullptr), timers(nullptr) {}
    
    void init(GraphType* g, Timers& t) {
        graph = g;
        timers = &t;
        
        size_t n = graph->getNumVertices();
        colorSet.resize(n);
        for (auto& cc : colorSet) {
            cc.reserve(n);
        }
    }
    
    void assignVertexNumber(VertexSet&, NumberedSet& ns, size_t i, VertexId vert, VertexId num) {ns[i] = std::make_pair(vert, num);}
    
    bool notEmpty(const NumberedSet& ns) const {return ns.size() > 0;}
    
    VertexId topNumber(const NumberedSet& ns) const {return ns.back();}
    
    VertexId topVertex(const NumberedSet& ns, const VertexSet&) const {return ns.backPair().first;}
    
    void popTop(NumberedSet& ns, VertexSet& vs) {auto v = ns.backPair().first; ns.pop_back(); vs.remove(v);}
    
    // take clique c and candidate vertex set p as input
    // return numbered vertex set np as output
    // difference compared to regular GreedyColorSort: p is here only used as input
    void numberSort(const VertexSet& c, const VertexSet& p, NumberedSet& np, unsigned int maxSize = 0) {
        Ubb = p;
        Qbb = Ubb;
        int k = 0;
        size_t i = 0;
        int kMin = (int)maxSize - (int)c.size();
        np.resize(p.size());
        
        while (Ubb.size() > 0) {
            while (Qbb.size() > 0) {
                auto v = Qbb.firstSetBit();
                Qbb &= graph->invAdjacencyMatrix[v];
                Qbb.recount();
                Ubb.remove(v);
                if (k >= kMin) {
                    np[i].first = v;
                    np[i++].second = k+1;
                }
            }
            Qbb = Ubb;
            ++k;
        }
        np.resize(i);
    }
    
    void initialSort(VertexSet& c, VertexSet& vertices, NumberedSet& color) {
        vertices.recount();
        numberSort(c, vertices, color);
    }

protected:    
    bool intersectionExists(VertexId p, const VertexSet& vertices) const {
        const VertexSet& neighbourhood = graph->adjacencyMatrix[p];
        auto intersection = neighbourhood;
        intersection &= vertices;
        return intersection.size() > 0;
    }
};
REGISTER_TEMPLATE1_CLASS_NAME(BBGreedyColorSort, "greedy color sort on bitstrings");

#endif // BB_GREEDYCOLORSORT_H_INCLUDED
