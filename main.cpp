#include <iostream>
#include <bitset>
#include <cassert>

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// User provided definitions that change how the program will compile
//
// defines used by the Max-clique algorithm:
// TRACE_LEVEL      ... define the level of trac-type logging
// TRACE_MASK       ... define which functionalities to trace
// NO_TIMING        ... define to disable timing (comment out to enable timing), which can be overwhelming on some architectures (e.g. Threadripper)
//
    #define TRACE_LEVEL 10
    #define TRACE_MASK 0
    //#define TRACE_MASK TRACE_MASK_CLIQUE
    //#define NO_TIMING
//
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include <AnsiTerminal.h>
#include "CommandLineParameters.h"

#include "Timers.h"

#include "Dimacs.h"
#include "Dimacs.h"
#include "SipGraphLoader.h"
#include "DatGraphLoader.h"
#include "ArgGraphLoader.h"
#include "MaximumCliqueBase.h"
#include "ParallelMaximumClique.h"
#include "GreedyColorSort.h"
#include "GreedyColorSort2.h"
#include "SteadyGreedyColorSort.h"
#include "BitstringSet.h"
#include "BB_GreedyColorSort.h"
#include "BB_ColorRSort.h"
#include "DegreeSort.h"
#include "DegreeAndNumberSort.h"
#include "MinWidthSort.h"
#include "Mcr.h"
#include "McrBB.h"
#include "Re_NumberSort.h"
#include "Graph.h"
#include "VectorSet.h"

#include "Mcq.h"
#include "Mcs.h"
//#include "Bbmc.h"


/*
    Configure color console output (loging and debugging) if developing on Linux
*/
#ifdef __linux
class workerEfficiency;
    const bool coloredOutput = true;
#else
    const bool coloredOutput = false;
    #define DISABLE_FANCY_TERMINAL
#endif

/**
 * @brief Calculate graph density from the number of vertices @link #v and number of edges @link e
 * @param v number of vertices
 * @param e number of edges
 * @return density of the graph (ratio of connectedness, 0 - not connected at all, 1 - fully connected)
 */
float getDensity(int v, int e) {
    return e*2.0/(v*(v-1.0));
}

/**
 * @brief Load a graph from the file provided by a file name 
 * @param fname a file name string
 * @return a Graph on success; throws on error
 */
template<typename NodeSet>
Graph<NodeSet> loadGraph(const char* fname) {
    if (coloredOutput) std::cout << FancyColor(0, 0.5, 0.5);
    std::cout << "Loading graph " << fname;
    if (coloredOutput) std::cout<< TermStyle();
    std::cout << std::endl;
    Graph<NodeSet> graph;
    std::stringstream error;
    
    // a do loop is used to support break statement, after a successful load attempts
    do {
        auto tryLoad=[&error, fname, &graph](GraphLoader&& loader, const char* loaderName) -> bool {
            if (loader.load(fname) && (loader.getNumVertices() > 0)) {
                graph.init(loader.getAdjacencyMatrix(), loader.getDegrees());
                std::cout << "  this is a " << loaderName << " file with a graph of " << loader.getNumVertices() << " vertices, " << loader.getNumEdges() << " edges, " 
                    << getDensity(loader.getNumVertices(), loader.getNumEdges()) << " density" << std::endl;
                graph.wasRemapedTo0based = loader.verticesAreMappedFrom1based();
                return true;
            } else {
                error << "\n   " << loaderName << " loader error: " << loader.getError();
                return false;
            }
        };
        
        // Try dimacs first
        if (tryLoad(Dimacs{}, "Dimacs"))
            break;
        // then 'SIP'
        if (tryLoad(SipGraphLoader{}, "SIP"))
            break;
        // then .dat
        if (tryLoad(DatGraphLoader{}, "Dat"))
            break;
        // and lastly Arg
        if (tryLoad(ArgGraphLoader{}, "Arg"))
            break;
        
                    
        //no loader could load the file
        throw std::runtime_error("Unable to load graph: "+error.str()+"\n");
    } while (false);

    
    return graph;
}


        typedef int VertexId;
        typedef VectorSet<VertexId> NodeSet;
        typedef GreedyColorSort<Graph<NodeSet>> ColorSort;
        template<class T>
        using InitialSort = DegreeSort<T>;
        /*
        typedef BitstringSet NodeSet;
        typedef BBGreedyColorSort<Graph<NodeSet>> ColorSort;
        template<class T>
        using InitialSort = DegreeAndNumberSort<T>;
        */
 

void pBbmcTest(const char* params, int numThreads, int numJobs, std::vector<int>& affinities, long timeout, bool invertInputGraph) {
    try {        
        const char* testCliqueFile = ( params == nullptr ? "12345.clq" : params);
        
        Graph<NodeSet> graphB = loadGraph<NodeSet>(testCliqueFile);
        bool graphLoaded = graphB.getNumEdges() > 0;
        if (!graphLoaded) {
            throw std::runtime_error("Unable to load graph");
        }
        if (invertInputGraph) {
            graphB.invertEdges();
            std::cout << "Inverted graph: " << graphB.getNumVertices() << " vertices " << graphB.getNumEdges() << " edges " 
            << graphB.getNumEdges()*2.0/(graphB.getNumVertices()*(graphB.getNumVertices()-1)) << " density " << std::endl;
        }
        
        PrecisionTimer t;
        bool isCliqueBool = false;
        NodeSet theClique;
        {
            ScopeTimer sc(t);
            if (numThreads == 0) {        
                MaximumCliqueProblem<
                    VertexId,                   // vertex ID
                    NodeSet,                    // vertex set
                    Graph<NodeSet>,             // graph
                    //BBMcrSort<BBColorRSort<Graph<BitstringSet>>>        // color sort
                    ColorSort,        // color sort
                    InitialSort 					// initial sort
                    > problem(graphB);
                problem.maxAllowedSearchTime = timeout;
                problem.search();
                problem.outputStatistics(std::cout, coloredOutput); std::cout << "\n\n"; 
                isCliqueBool = isClique(graphB, problem.getClique());
                theClique = problem.getClique();
            } else {
                ParallelMaximumCliqueProblem<
                    VertexId,                        // vertex ID
                    NodeSet,               // vertex set
                    Graph<NodeSet>,        // graph
                    //BBMcrSort<BBColorRSort<Graph<BitstringSet>>>,        
                    ColorSort,        // color sort
                    InitialSort        // initial sort
                    //DegreeSort
                    > problemP1(graphB);
                problemP1.maxAllowedSearchTime = timeout;
                problemP1.search(numThreads, numJobs, affinities);
                problemP1.outputStatistics(std::cout, coloredOutput); std::cout << "\n"; 
                std::cout << "Thread efficiency = " << std::setprecision(3) << std::max(0.0, problemP1.workerEfficiency()) << "\n";
                isCliqueBool = isClique(graphB, problemP1.getClique());
                theClique = problemP1.getClique();
            }
        }
        if (!isCliqueBool) {
            if (coloredOutput)
                std::cerr << FancyColor(1, 0, 0);
            std::cout << "Algorithm error: found set of vertices is not a clique" << std::endl;
            if (coloredOutput) 
                std::cerr << TermStyle() << std::flush;
        }
        
        std::cout << "Total time = " << t.lastSeconds() << " seconds\n";
    } catch (std::exception& e) {
        std::cout << "Terminated due to exception: ";
        if (coloredOutput)
            std::cerr << TermStyle::Underlined() << FancyColor(1, 0, 0);
        std::cerr << e.what() << std::endl;
        if (coloredOutput) 
            std::cerr << TermStyle() << std::flush;
    } catch (...) {
        std::cerr << "Terminated due to ";
        if (coloredOutput)
            std::cerr << TermStyle::Underlined() << FancyColor(1, 0, 0);
        std::cerr << "unknown";
        if (coloredOutput) 
            std::cerr << TermStyle();
        std::cerr << " exception: " << std::endl;
    }
    std::cout << std::endl;
}



int main(int argc, char** argv) {
    try {
        // load commandline arguments (also generate graphs or load them from file, as required)
        using namespace CommandlineParameters;
        ParameterSet parameterSet;
        bool debugMC;
        bool printHelp = false;
        long timeout = 1000;
        std::string inputString;    // this is a dummy variable, reused by parsing function and will not hold any useful value after parsing commandline
        std::vector<std::string> inputGraphParameters;
        std::vector<int> bindProcessors;
        int numThreads = 0, numJobs = 1;
        bool invertInputGraph = false;

        std::cout << "warning: this algorithm does not use bitboards\n";
        
        parameterSet.addDefinition("-timeout", "Timeout in seconds; bound the maximum execution time of the algorithm.")
            .setNumberOfValues(1)
            .bindToVariable(timeout)
            .setDefaultValue(1000);
        parameterSet.addDefinition("-input", "Provides a graph input to the algorithm, in a form of a file (path is provided); supported file formatas are dimacs text, SIP, Arg, and .dat.")
            .setNumberOfValues(1)
            .bindToVariable(inputString)
            .addOnChangeHandler([&inputGraphParameters](const std::string& val)->std::string {inputGraphParameters.push_back(val); return "";});
        parameterSet.addDefinition("-threads", "Defines the number of threads to execute on.")
            .setDefaultValue("0")     // 0 = sequential algorithm
            .setNumberOfValues(1)
            .bindToVariable(numThreads);
        parameterSet.addDefinition("-jobs", "Defines the number of threads to execute on.")
            .setDefaultValue("1")     // 0 = sequential algorithm
            .setNumberOfValues(1)
            .bindToVariable(numJobs);
        parameterSet.addDefinition("-bind", "Defines how the threads should be bound to cores: takes a list of core indices, separated by commas.")
            .setNumberOfValues(1)
            .bindToVariable(inputString)
            .addOnChangeHandler([&bindProcessors](const std::string& val)->std::string {
                std::string singleIndex;
                std::stringstream valStream(val);
                while(getline(valStream, singleIndex, ',')) 
                    bindProcessors.push_back(std::stoi(singleIndex));
                return "";}
            );
        parameterSet.addDefinition("-invertInput", "Inverts the graph before starting the search; effectively this changes max cliuqe to max independent set algorithm.")
            .setNumberOfValues(0)
            .bindToVariable(invertInputGraph);
        parameterSet.addDefinition("-help", "Show the help screen.")
            .setDefaultValue("false")
            .bindToVariable(printHelp);
        parameterSet.addDefinition("--help", "Show the help screen.")
            .makeHelpInvisible()
            .setDefaultValue("false")
            .bindToVariable(printHelp);
        
        // TODO add all missing definitions
                
        // parse the parameters
        try {
            auto r = parameterSet.parse(argv+1, argc-1); 
            
            // check if parameters were added at all
            if (printHelp || (argc == 0)) {
                std::cerr << "This program should be provided the following arguments:\n";
                std::cerr << parameterSet.generateHelpScreen();
                return true;
            }
            
            if (r.errors) {
                std::cout << "Error while parsing arguments: " << parameterSet.getLog() << std::endl; 
                return false;
            }
        } catch (...) {
            std::cout << "Exception while parsing parameters.\n";
            throw;
        }
        
        if (inputGraphParameters.size() == 1)
            pBbmcTest(inputGraphParameters[0].c_str(), numThreads, numJobs, bindProcessors, timeout, invertInputGraph);
        else {
            if (inputGraphParameters.size() > 1)
                std::cout << "Error: number of input graphs must be 1." << std::endl;
            else
                std::cout << "Error: no input graphs were provided." << std::endl;
        }
        //unitTests(argc > 1 ? argv[1] : nullptr, numThreads, bindProcessors);
        //bbmcTest(argc > 1 ? argv[1] : nullptr, numThreads, numJobs, bindProcessors);
    } catch (const std::exception& e) {
        std::cout << "Terminated due to exception: " << e.what() << std::endl;
    } catch (const std::string& e) {
        std::cout << "Terminated due to exception: " << e << std::endl;
    } catch (...) {
        std::cout << "Terminated due to unknown exception: " << std::endl;
    }
    return 0;
}
