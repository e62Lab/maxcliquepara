#ifndef MCS_H
#define MCS_H


#include "Mcq.h"


// MCS is MCQ with a static color ordering set at the top of search (MCSa)
class Mcs : public Mcq {
public:
    Mcs();
    Mcs(int n, const std::vector<std::vector<int> >& A, const std::vector<int>& degree, int sortingStyle = sort_degree);
    void search();
    void expand(VertexList& C, VertexList& P, VertexList& ColOrd);
};


// MCS_Cr is MCS with the color repair mechanism. (MCSb)
class McsCr : public Mcs {
public:
    McsCr();
    McsCr(int n, const std::vector<std::vector<int> >& A, const std::vector<int>& degree, int sortingStyle = sort_degree);
    void search();
    void expand(VertexList& C, VertexList& P, VertexList& ColOrd);
    void numberSort(VertexList& C, VertexList& ColOrd, VertexList& P, VertexList& color);
    int getSingleConflictVariableIndex(int v, VertexList& colorClass);
    bool repair(int v, int k);
};


#endif // MCS_H
