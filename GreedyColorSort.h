#ifndef GREEDYCOLORSORT_H
#define GREEDYCOLORSORT_H


#include "MaximumCliqueBase.h"


template<class Graph>
class GreedyColorSort {
public:
    typedef typename Graph::VertexSet VertexSet;
    typedef typename VertexSet::VertexId VertexId;
    typedef std::vector<VertexId> NumberedSet;
    typedef Graph GraphType;
    
protected:
    std::vector<VertexSet> colorSet;
    Graph* graph;
    Timers* timers;
    
public:
    GreedyColorSort() : graph(nullptr) {}
    
    void init(GraphType* g, Timers& t) {
        graph = g;
        timers = &t;
        size_t n = graph->getNumVertices();
        colorSet.resize(n);
        for (size_t i = 0; i < n; ++i)
            colorSet[i].reserve(n);
    }
    
    void assignVertexNumber(VertexSet& vs, NumberedSet& ns, size_t i, VertexId vert, VertexId num) {vs[i] = vert; ns[i] = num;}
    
    bool notEmpty(const NumberedSet& ns) const {return ns.size() > 0;}
    
    VertexId topNumber(const NumberedSet& ns) const {return ns.back();}
    
    VertexId topVertex(const NumberedSet& ns, const VertexSet& vs) const {return vs.back();}
    
    void popTop(NumberedSet& ns, VertexSet& vs) {ns.pop_back(); vs.pop();}
    
    /**
     * @brief Take inputs clique #c, vertex set #p, and maximum size(?) #maxSize as inputs, generate a colored vertex set #np as output
     * @param c         input clique
     * @param p         input vertex set
     * @param np        input
     * @param maxSize
     */
    // return numbered vertex set np as output
    void numberSort(const VertexSet& c, VertexSet& p, NumberedSet& np, unsigned int maxSize = 0) {
        size_t m = p.size();
            
        for (size_t i = 0; i < m; i++) {
            VertexId v = p[i];
            size_t k = 0;
            
            while (intersectionExists(v, colorSet[k])) 
                k++;
                
            colorSet[k].add(v);
        }
        
        np.resize(m);
        for (size_t k = 0, i = 0; k < colorSet.size(); k++) {
            if (colorSet[k].empty())
                break;
            for (size_t j = 0; j < colorSet[k].size(); j++) {
                VertexId v = colorSet[k][j];
                p[i] = v; 
                np[i++] = k+1;
            }
            colorSet[k].clear();
        }
    }

protected:    
    bool intersectionExists(VertexId p, const VertexSet& vertices) const {
        const VertexSet& neighbourhood = graph->adjacencyMatrix[p];
        size_t n = vertices.size();
        for (size_t i = 0; i < n; ++i) {
            if (neighbourhood[vertices[i]] == true)
                return true;
        }
        return false;
    }
};
REGISTER_TEMPLATE1_CLASS_NAME(GreedyColorSort, "Greedy color sort");

#endif // GREEDYCOLORSORT_H
