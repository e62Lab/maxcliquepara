#include "Mcq.h"

#include "Mcq.h"
#include <algorithm>


Mcq::Mcq() {
    algorithmName = "MCQ";
    sortingStyle = sort_degree;
}

Mcq::Mcq(int n, const std::vector<std::vector<int> >& A, const std::vector<int>& degree, int sortingStyle) :
      MaximumCliqueBase(n, A, degree)  
{
    algorithmName = "MCQ";
    this->sortingStyle = sortingStyle;
    timers.set(timer_impl2, "intersection", timer_color_sort);
}

void Mcq::search() {
    timers.reset();
    ScopeTimer tm(timers[timer_total]);
    VertexList P, C;
    {
        ScopeTimer tm1(timers[timer_init]);
        countExpands = 0;
        C.reserve(A.size());            // clique
        // up to here - copy of base class
        
        P.reserve(A.size());             // candidate countExpands of graph
        
        colorClass.resize(n);
        for (auto& cc : colorClass)
            cc.reserve(n);
            
        orderVertices(P);
    }
    ScopeTimer tm2(timers[timer_search]);
    /*// DEBUG
    for (size_t i = 0; i < n; ++i)
        std::cout << P[i] << " "; //*/
    
    expand(C,P);
}

void Mcq::expand(VertexList& C, VertexList& P) {
    countExpands++;
    
    // change compared to base: [
    size_t m = P.size();
    VertexList color(m);
    numberSort(P, P, color);
    // ]
        
    for (int i = (int)m-1; i >= 0; i--){
        if (C.size() + color[i] <= maxSize) return; // [change]
        int v = P[i];
        VertexList newP;
        newP.reserve(P.size());
        P.pop_back();
        
        // intersection of P and neighbours of v
        for (int j = 0; j <= i; ++j)
            if (A[v][P[j]] == 1) 
                newP.push_back(P[j]);
                
        C.push_back(v);
        if (newP.size() == 0) {
            if (C.size() > maxSize) 
                saveSolution(C);
        } else {
            expand(C, newP);
        }
        C.pop_back();
    }
}

bool Mcq::conflicts(int v, VertexList& colors) {
	for (size_t i = 0; i < colors.size(); i++) {
	    int w = colors[i];
	    if (A[v][w] == 1) return true;
	}
	return false;
}

void Mcq::orderVertices(VertexList& ColOrd) {
    ScopeTimer tm(timers[timer_initial_sort]);
    EVertexList V(n);
    for (int i = 0; i < n; i++) 
        V[i].init(i, degree[i]);
        
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++) 
            if (A[i][j] == 1) V[i].nebDeg = V[i].nebDeg + degree[j];
            
    if (sortingStyle == sort_degree) // default sorting style
        std::sort(V.begin(), V.end());
    else if (sortingStyle == sort_minWidth) 
        minWidthOrder(V);
    else if (sortingStyle == sort_mcr) {
        std::sort(V.begin(), V.end(), 
            [](const ExtendedVertex& v1, const ExtendedVertex& v2) {
                return (v1.degree > v2.degree || (v1.degree == v2.degree && v1.nebDeg > v2.nebDeg) ||
                    (v1.degree == v2.degree && v1.nebDeg == v2.nebDeg && v1.index < v2.index));
            }
        );
    }
    ColOrd.resize(V.size());
    for (size_t i = 0; i < V.size(); ++i) {
        ColOrd[i] = V[i].index;
    }
    
}

void Mcq::minWidthOrder(EVertexList& V) {
	EVertexList L;
	L = V;
	int k = L.size() - 1;
	while (L.size() > 0) {
	    size_t i = 0;
	    for (size_t j = 1; j < L.size(); ++j) 
            if (L[j].degree < L[i].degree) i = j;
	    V[k] = L[i];
        L.erase(L.begin()+i);
        // this line can be used instead of the previous one (but reorders nodes): std::swap(L[i], L.back()); L.pop_back();
	    for (ExtendedVertex& u : L) 
            if (A[u.index][V[k].index] == 1)
                u.degree--;
        --k;
	}
}

void Mcq::numberSort(VertexList& ColOrd, VertexList& P, VertexList& color) {
    ScopeTimer tm(timers[timer_color_sort]);
	size_t colors = 0;
	int m = ColOrd.size();
	
	for (int i = 0; i < m; i++) 
        colorClass[i].clear();
        
	for (int i = 0; i < m; i++){
	    int v = ColOrd[i];
	    size_t k = 0;
        {
         //   ScopeTimer sct(timers[timer_impl2]);
            while (conflicts(v, colorClass[k])) 
                k++;
        }
	    colorClass[k].push_back(v);
	    colors = std::max(colors, k+1);
	}
	
	P.resize(m);
	int i = 0;
	for (size_t k = 0; k < colors; k++) {
	    for (size_t j = 0; j < colorClass[k].size(); j++) {
            int v = colorClass[k][j];
            P[i] = v; 
            color[i++] = k+1;
	    }
	}
}
