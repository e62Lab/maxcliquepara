#ifndef ANSI_TERMINAL_H
#define ANSI_TERMINAL_H


#include <cmath>


#include <iostream>
#include <algorithm>
#include <cstdio>
#include <unistd.h> // gcc 4.7 fails to find STDIN_FILENO, if not including <unistd.h> 
#ifdef __linux
    #include <sys/ioctl.h>
#endif


struct TermStyle {
    enum {black, red, green, yellow, blue, magenta, cyan, white};
    enum {foreground = 30, background = 40, bold = 1, faint = 2, italic = 3, underlined = 4, blink = 5, reverse = 7};
    int number;
    
    TermStyle() :                               number(0) {}
    TermStyle(int n, int style=foreground) :    number(n+style) {}
    
    static TermStyle Bold() {return TermStyle(0, bold);}
    static TermStyle Faint() {return TermStyle(0, faint);}
    static TermStyle Italic() {return TermStyle(0, italic);}
    static TermStyle Underlined() {return TermStyle(0, underlined);}
    static TermStyle Blink() {return TermStyle(0, blink);}
    static TermStyle Reverse() {return TermStyle(0, reverse);}
    static TermStyle Foreground(int col=white) {return TermStyle(col % 8, foreground);}
    static TermStyle Background(int col=black) {return TermStyle(col % 8, background);}
};


struct FancyColor {    
    // http://www.pixelbeat.org/docs/terminal_colours/
    int number, code;
    
    FancyColor(int col = 0, bool fore = true) : number(col), code(fore ? 38 : 48) {}
    FancyColor(float r, float g, float b, bool fore = true) : code(fore ? 38 : 48) {
        number = 16 + floor(b*5+0.5) + 6 * (floor(g*5+0.5) + 6 * floor(r*5+0.5));
    }
    
    const FancyColor& operator=(const FancyColor& other) {
        number = other.number;
        code = other.code;
        return *this;
    }
};


struct AnsiTerminal {
    static void getSize(int& cols, int& lines) {
    #ifdef TIOCGSIZE
        struct ttysize ts;
        ioctl(STDIN_FILENO, TIOCGSIZE, &ts);
        cols = ts.ts_cols;
        lines = ts.ts_lines;
    #elif defined(TIOCGWINSZ)
        struct winsize ts;
        ioctl(STDIN_FILENO, TIOCGWINSZ, &ts);
        cols = ts.ws_col;
        lines = ts.ws_row;
    #else
        throw "getTermSize is not supported on this system";
    #endif /* TIOCGSIZE */
    }
};

#ifndef DISABLE_FANCY_TERMINAL
template <class A, class B>
std::ostream& operator<< (std::basic_ostream<A, B>& out, TermStyle mod) {
    return out << "\033[" << mod.number << 'm';
}

template <class A, class B>
std::ostream& operator<< (std::basic_ostream<A, B>& out, FancyColor col) {
    return out << "\033[" << col.code << ";5;" << col.number << 'm';
}
#else 
template <class A, class B>
std::ostream& operator<< (std::basic_ostream<A, B>& out, TermStyle mod) {
    return out;
}

template <class A, class B>
std::ostream& operator<< (std::basic_ostream<A, B>& out, FancyColor col) {
    return out;
}
#endif


#endif // ANSI_TERMINAL_H
