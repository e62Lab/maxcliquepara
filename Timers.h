#ifndef TIMERS_H
#define TIMERS_H


#include <Timer.h>
#include <vector>
#include <cassert>


struct Timers {
    struct TimerNode {
        PrecisionTimer timer;
        mutable unsigned long int count = 0, childCount = 0;
        mutable unsigned long int extra = 0;
        int mark;
        int parent = -1;
        int hierarchyDepth = 0;
        std::vector<int> children;
        std::string name;
    };
    
    std::vector<TimerNode> nodes;
    mutable double timerOverheadPerCall, timerInaccuracy;
    mutable bool hierarchyOk;
    
    Timers();
    Timers(const Timers& other) = default;
    const Timers& operator=(const Timers& other);
    // add a new timer to hierarchy
    void set(int id, const std::string& name, int parent = -1);
    void setTree(int destinationRootId, const Timers& tree, int treeRootId);
    size_t size() const {return nodes.size();}
    // resets numbers but not names and hierarchy
    void reset();
    
    void mark(int i, int j=1) {nodes[i].mark = j;}
    void unmark(int i) {nodes[i].mark = 0;}
    
    // add times from another timers object (useful when combining timers form different threads)
    // will skip marked timers
    void addTimes(const Timers& other);
    // specify the number of repetitions of time measurements from which time measurement overhead is calculated 
    void estimateTimerOverhead(size_t numRepeats = 10000) const;
    void resolveHirearchy() const;
    void printReport(std::ostream& out, bool colored=true) const;
    PrecisionTimer& operator[] (size_t i) {++nodes[i].count; return nodes[i].timer;} // call this when timing
    const PrecisionTimer& operator() (int i) {return nodes[i].timer;}           // call this to view results
    void addExtra(size_t i, long extra) {nodes[i].extra += extra;}      // call this to add to the 'extra' variable
    
    double seconds(size_t i, bool minusTimer = false) const;
    int getCount(size_t i) const {
        return nodes[i].count;
    }
    
    void linkChildToParent(size_t child, size_t parent);
    
    void getTimeAndCount(double& time, int& count, unsigned int index) const {
        assert(index < nodes.size()); 
        time = nodes[index].timer.totalSeconds(); 
        count = nodes[index].count;
    }
};


#endif // TIMERS_H
