#include "Mcs.h"

Mcs::Mcs() {
    algorithmName = "MCSa";
}


Mcs::Mcs(int n, const std::vector<std::vector<int> >& A, const std::vector<int>& degree, int sortingStyle) : Mcq(n, A, degree, sortingStyle)  {
    algorithmName = "MCSa";
}

void Mcs::search(){
    timers.reset();
	ScopeTimer tm(timers[timer_total]);
    VertexList P, C, ColOrd;
    {
        ScopeTimer tm1(timers[timer_init]);
        countExpands = 0;
        C.reserve(A.size());            // clique
        
        P.reserve(A.size());            // candidate countExpands of graph
        
        // first one of the different lines
        ColOrd.reserve(A.size());       // ?
        
        colorClass.resize(n);
        for (auto& cc : colorClass)
            cc.reserve(n);
        
        // the second one of the different lines
        orderVertices(ColOrd);
    }
    ScopeTimer tm2(timers[timer_search]);
    expand(C, P, ColOrd);
}

void Mcs::expand(VertexList& C, VertexList& P, VertexList& ColOrd) {
    countExpands++;
    
    int m = ColOrd.size();   // difference []
    VertexList color(m);
    numberSort(ColOrd, P, color); // difference []
        
    for (int i = m-1; i >= 0; --i){
        if (C.size() + color[i] <= maxSize) return; // [change]
        int v = P[i];
        int vIndexInColOrd = -1;
        VertexList newP, newColOrd;
        newP.reserve(P.size());
        newColOrd.reserve(P.size());
        P.pop_back();
        
        // intersection of P and neighbours of v
        for (int j = 0; j <= i; ++j) {
            if (A[v][P[j]] == 1) 
                newP.push_back(P[j]);
            if (A[v][ColOrd[j]] == 1) 
                newColOrd.push_back(ColOrd[j]);
            if (ColOrd[j] == v)
                vIndexInColOrd = j;
        }
                
        C.push_back(v);
        if (newP.size() == 0) {
            if (C.size() > maxSize) 
                saveSolution(C);
        } else {
            expand(C, newP, newColOrd);
        }
        C.pop_back();
        ColOrd.erase(ColOrd.begin() + vIndexInColOrd);
    }
}


McsCr::McsCr() {
    algorithmName = "MCSb";
}


McsCr::McsCr(int n, const std::vector<std::vector<int> >& A, const std::vector<int>& degree, int sortingStyle) : Mcs(n, A, degree, sortingStyle)  {
    algorithmName = "MCSb";
}

void McsCr::search(){
	ScopeTimer tm(timers[timer_total]);
    VertexList P, C, ColOrd;
    {
        ScopeTimer tm1(timers[timer_init]);
        countExpands = 0;
        C.reserve(A.size());            // clique
        
        P.reserve(A.size());            // candidate countExpands of graph
        
        // first one of the different lines
        ColOrd.reserve(A.size());       // ?
        
        colorClass.resize(n);
        for (auto& cc : colorClass)
            cc.reserve(n);
        
        // the second one of the different lines
        orderVertices(ColOrd);
    }
    ScopeTimer tm2(timers[timer_search]);
    expand(C, P, ColOrd);
}

void McsCr::expand(VertexList& C, VertexList& P, VertexList& ColOrd) {
    countExpands++;
    
    int m = ColOrd.size();
    VertexList color(m);
    numberSort(C, ColOrd, P, color); // difference []
        
    for (int i = m-1; i >= 0; i--){
        if (C.size() + color[i] <= maxSize) return; // [change]
        int v = P[i];
        int vIndexInColOrd = -1;
        VertexList newP, newColOrd;
        newP.reserve(P.size());
        newColOrd.reserve(P.size());
        P.pop_back();
        
        // intersection of P and neighbours of v
        for (int j = 0; j <= i; ++j) {
            if (A[v][P[j]] == 1) 
                newP.push_back(P[j]);
            if (A[v][ColOrd[j]] == 1) 
                newColOrd.push_back(ColOrd[j]);
            if (ColOrd[j] == v)
                vIndexInColOrd = j;
        }
                
        C.push_back(v);
        if (newP.size() == 0) {
            if (C.size() > maxSize) 
                saveSolution(C);
        } else {
            expand(C, newP, newColOrd);
        }
        C.pop_back();
        ColOrd.erase(ColOrd.begin() + vIndexInColOrd);
    }
}

void McsCr::numberSort(VertexList& C, VertexList& ColOrd, VertexList& P, VertexList& color) {
    ScopeTimer tm(timers[timer_color_sort]);
	int delta   = maxSize - C.size();
	int colors  = 0;
	int m       = ColOrd.size();
	
	for (int i = 0; i < m; i++) 
        colorClass[i].clear();
        
	for (int i = 0; i < m; i++) {
	    int v = ColOrd[i];
	    int k = 0;
	    while (conflicts(v, colorClass[k])) 
            k++;
	    colorClass[k].push_back(v);
	    colors = std::max(colors, k+1);
	    if (k+1 > delta && colorClass[k].size() == 1 && repair(v, k)) 
            colors--;
	}
	
	P.clear();
	int i = 0;
	for (int k = 0; k < colors; k++) {
	    for (size_t j = 0; j < colorClass[k].size(); j++) {
            int v = colorClass[k][j];
            P.push_back(v); 
            color[i++] = k+1;
	    }
    }
}

int McsCr::getSingleConflictVariableIndex(int v, VertexList& colors) {
	int conflictVari = -1;
	int count       = 0;
	for (size_t i = 0; i < colors.size() && count<2; i++) {
	    int w = colors[i];
	    if (A[v][w] == 1) {
            conflictVari = i; 
            count++;
        }
	}
	return (count > 1 ? -count : conflictVari);
}

bool McsCr::repair(int v, int k) {
	for (int i = 0; i < k-1; i++) {
	    int wi = getSingleConflictVariableIndex(v, colorClass[i]);
	    if (wi >= 0) {
            int w = colorClass[i][wi];
            for (int j = i+1; j < k; j++) {
                if (!conflicts(w, colorClass[j])) {
                    colorClass[k].erase(std::find(colorClass[k].begin(), colorClass[k].end(), v));
                    colorClass[i][wi] = v;
                    colorClass[j].push_back(w);
                    return true;
                }
            }
        }
    }
    return false;
}
