#include "BitstringSet.h"


void fillWithRange(BitstringSet& b, int min, int max) {
    b.resize(max);
    b.set(min, max);
    b.recount();
}
