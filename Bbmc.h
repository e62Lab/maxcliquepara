#ifndef BBMC_H
#define BBMC_H


#include "Mcq.h"
#include "BitSet.h"


class Bbmc : public Mcq {
    std::vector<BitSet> N;    // neighbourhood
    std::vector<BitSet> invN; // inverse neighbourhood
    std::vector<ExtendedVertex> V;    // mapping bits to vertices

public:
    Bbmc();
    Bbmc(int n, const std::vector<std::vector<int> >& A, const std::vector<int>& degree, int sortingStyle = sort_degree);
    void search();
    void expand(BitSet& C, size_t Csize, BitSet& P, size_t Psize);
    void numberSort(BitSet& P, size_t Psize, VertexList& U, VertexList& color);
    void orderVertices();
    void saveSolution(const BitSet& C);
};


#endif // BBMC_H
