#include "MaximumCliqueBase.h"

/*
Exact Algorithms for Maximum Clique Problem (MCP): We can address the decision and optimization problems with
an exact algorithm, such as a backtracking search. Backtracking search incrementally constructs the set C 
(initially empty) by choosing a candidate vertex
from the candidate set P (initially all of the vertices in V ) and then adding it to C. Having cho-
sen a vertex the candidate set is then updated, removing vertices that cannot participate in the
evolving clique. If the candidate set is empty then C is maximal (if it is a maximum we save it)
and we then backtrack. Otherwise P is not empty and we continue our search, selecting from P
and adding to C.

There are other scenarios where we can cut off search, i.e. if what is in P is insufficient to
unseat the champion (the largest clique found so far) search can be abandoned. That is, an upper
bound can be computed. Graph colouring can be used to compute an upper bound during search,
i.e. if the candidate set can be coloured with k colours then it can contain a clique no larger than
k. There are also heuristics that can be used when selecting the candidate
vertex, different styles of search, different algorithms to colour the graph and different orders in
which to do this.
*/

MaximumCliqueBase::MaximumCliqueBase() {
}

MaximumCliqueBase::MaximumCliqueBase(int n, const std::vector<std::vector<int> >& A, const std::vector<int>& degree) {
    algorithmName = "Basic maximum clique";
    this->n = n;
    this->A = A;
    this->degree = degree;
    countExpands = 0;
    maxSize = 0;

    timers.set(timer_total, "total");
    timers.set(timer_init, "initialization", timer_total);
    timers.set(timer_search, "clique search", timer_total);
    timers.set(timer_initial_sort, "intitial sorting", timer_init);
    timers.set(timer_color_sort, "coloring & sorting", timer_search);
}

// note: not used in extended clique searches, only here as the basic reference
void MaximumCliqueBase::search() {
    timers.reset();
    ScopeTimer tm(timers[timer_total]);
    VertexList P, C;
    {
        ScopeTimer tm1(timers[timer_init]);
        countExpands = 0;
        C.reserve(A.size());            // clique
        P.resize(A.size());             // candidate countExpands of graph
        for (int i=0; i<n; i++) 
            P[i] = i;
    }
    ScopeTimer tm2(timers[timer_search]);
    expand(C,P);
}

void MaximumCliqueBase::outputStatistics(std::ostream& out, bool colored) {
    out << "-- " << algorithmName << " ";
    for (int i=0; i < 75-(int)algorithmName.size(); ++i)
        out << '-';
    out << "\n";
    out << countExpands << " expands called\n";
    timers.printReport(out, colored);
    VertexList c = getClique();
    if (c.size() > 40) c.resize(40);
    out << "Clique (" << getClique().size() << "): " << c;
}

// note: not used in extended clique searches, only here as the basic reference
void MaximumCliqueBase::expand(VertexList& C, VertexList& P) {
    countExpands++;
    for (int i = P.size()-1; i >= 0; i--){
        if (C.size() + P.size() <= maxSize) return;
        int v = P[i];
        C.push_back(v);
        VertexList newP;
        newP.reserve(P.size());
        for (int w : P) 
            if (A[v][w] == 1) 
                newP.push_back(w);
        if (newP.size() == 0 && C.size() > maxSize) 
            saveSolution(C);
        if (!newP.size() == 0) 
            expand(C, newP);
        C.pop_back();
        P.pop_back();
    }
}

void MaximumCliqueBase::saveSolution(const VertexList& C){
    solution = C;
    maxSize = C.size();
}
