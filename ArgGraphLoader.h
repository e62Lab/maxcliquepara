#ifndef ARG_GRAPH_LOADER_H
#define ARG_GRAPH_LOADER_H


/**
 * This file contains the class for loading ARG library graphs, as provided here: 
 *  <http://mivia.unisa.it/datasets/graph-database/arg-database/>
 * 
 * Warning, loading graphs reduces the number of labels for the labelling to look like
 *  CP 2011 labelling scheme
 * 
 * File format is as follows: 
 *  - binary, all entries are 16-bit unsigned integers
 *  - first entry is the number of nodes
 *  - next are the labels for nodes
 *  - then for each node, there is:
 *      - number of edges
 *      - for each edge:
 *          - destination vertex for this edge
 *          - edge label
 */


#include <GraphLoader.h>
#include <GraphLabels.h>
#include <utility>
#include <vector>
#include <string>

#include <fstream>


/**
 * @brief This loader is used to load files from the ARG database, see here:
 * <http://mivia.unisa.it/datasets/graph-database/arg-database/>
 * 
 * Vertices are loaded into standard vector of vector of char adjacency matrix,
 * where only two values are allowed: true for existing connection and false for 
 * no connection.
 * 
 * Labels for vertices and edges are stored in GraphLabels base class instance
 */
class ArgGraphLoader : public GraphLoader, public GraphLabels<uint16_t> {
public:
    bool DEBUG_LOADER = false;
    
protected:
    /**
     * @brief Destination for reporting all errors when loading a file
     */
    std::string error;
    
public:    
    // overloaded funcition calls for the unlabelled and undirected graphs
    bool load(const char* fname) override;
    unsigned int getNumVertices() const override            { return vertexLabels.size(); };
    unsigned int getMaxVertiexIndex() const override        { return getNumVertices()-1; };
    unsigned int getNumEdges() const override;
    std::vector<std::vector<char> > getAdjacencyMatrix() const override;
    std::vector<int> getDegrees() const override;
    const std::string& getError() const override            { return error; }
    bool verticesAreMappedFrom1based() const override       { return true; }
    GraphLabels<uint16_t> getLabelling() const              { return *this; }

protected:
    
    /**
     * @brief File is composed of word records, this function reads a single record
     * @param infile
     * @return 
     */
    uint16_t readWord(std::ifstream& infile) {
        uint8_t a, b;
        a = static_cast<uint8_t>(infile.get());
        b = static_cast<uint8_t>(infile.get());
        return a + (uint16_t(b) << 8);
    }
};


#endif //ARG_GRAPH_LOADER_H
