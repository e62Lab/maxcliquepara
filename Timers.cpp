#include <iostream>
#include <iomanip>
#include <AnsiTerminal.h>
#include "Timers.h"


Timers::Timers() {
}

const Timers& Timers::operator=(const Timers& other) {
    nodes = other.nodes;
    timerOverheadPerCall = other.timerOverheadPerCall;
    timerInaccuracy = other.timerInaccuracy;
    hierarchyOk = other.hierarchyOk;
    return *this;
}

void Timers::set(int id, const std::string& name, int parent) {
    if (id > 1000)
        throw std::runtime_error("Please set timer id below 1000");
    if (parent >= (int)nodes.size())
        throw std::runtime_error("Please set parent timer node before its children");
    if ((int)nodes.size() < id+1)
        nodes.resize(id+1);
    nodes[id].name = name;
    nodes[id].parent = parent;
    nodes[id].hierarchyDepth = (parent >= 0 ? nodes[parent].hierarchyDepth+1 : 0);
    nodes[id].extra = 0;
    if (parent >= 0)
        nodes[parent].children.push_back(id);
}

void Timers::setTree(int destinationParentId, const Timers& tree, int treeRootId) {
    // all nodes from tree will be offset to fit into the destination
    int idOffset = std::max((int)nodes.size(), destinationParentId);
    int depthOffset = nodes[destinationParentId].hierarchyDepth;
    nodes.resize(tree.nodes.size()+idOffset);
    
    // create a stack of nodes to be added then add them in appropriate order (one that respects hierarchy)
    std::vector<int> nodeStack;
    nodeStack.reserve(tree.nodes.size());
    nodeStack.push_back(treeRootId);
    
    for (size_t i = 0; i < nodeStack.size(); ++i) {
        // work on node n (next on stack)
        int n = nodeStack[i];
        int nn = (i == 0 ? destinationParentId : n+idOffset);
        
        // copy n to nodes (but do not touch)
        nodes[nn].timer         = tree.nodes[n].timer;
        nodes[nn].count         = tree.nodes[n].count;
        nodes[nn].extra         = tree.nodes[n].extra;
        nodes[nn].mark          = tree.nodes[n].mark;
        nodes[nn].childCount    = tree.nodes[n].childCount;
        nodes[nn].children.clear();
        if (tree.nodes[n].children.size() > 0) {
            for (const int j : tree.nodes[n].children) {
                nodeStack.insert(nodeStack.begin() + i + 1, j);
                nodes[nn].children.push_back(j + idOffset);
            }
        }
        
        if (i != 0) {
            nodes[nn].name              = tree.nodes[n].name;
            nodes[nn].hierarchyDepth    = tree.nodes[n].hierarchyDepth + depthOffset;
            nodes[nn].parent            = tree.nodes[n].parent + idOffset;
            //std::cout << " adding " << nodes[nn].name << " as a link to " << nodes[nodes[nn].parent].name << "\n";
        }
    }
}

void Timers::reset() {
    for (auto& n : nodes) {
        n.timer.reset();
        n.count = n.childCount = 0;
        n.mark = 0;
        n.extra = 0;
    }
    timerOverheadPerCall = timerInaccuracy = 0.0;
    hierarchyOk = false;
}

void Timers::addTimes(const Timers& other) {
    size_t numTimers = std::min(nodes.size(), other.nodes.size());
    size_t countThis = 0, countOther = 0;
    int countTimers = 0;
    
    // add only the un-marked timers
    for (size_t i=0; i < numTimers; ++i) {
        if (nodes[i].mark == 0 && other.nodes[i].mark == 0) {
            nodes[i].timer += other.nodes[i].timer;
            countThis += nodes[i].count;
            countOther += other.nodes[i].count;
            nodes[i].count += other.nodes[i].count;
            nodes[i].extra += other.nodes[i].extra;
            ++countTimers;
        }
    }
    if (other.timerOverheadPerCall > 0) {
        timerOverheadPerCall = (timerOverheadPerCall*countThis + other.timerOverheadPerCall*countOther) / (countThis + countOther);
        timerInaccuracy = (timerInaccuracy*countThis + other.timerInaccuracy*countOther) / (countThis + countOther);
    }
}

// specify the number of repetitions of time measurements from which time measurement overhead is calculated 
void Timers::estimateTimerOverhead(size_t numRepeats) const {
    
    PrecisionTimer tm1, tm2;
    {
        volatile ScopeTimer t1_(tm1);
        for (size_t i = 0; i < numRepeats; ++i) {
            volatile ScopeTimer t2_(tm2);
        }
    }
    timerOverheadPerCall = tm1.totalSeconds() / numRepeats;
    timerInaccuracy = tm2.totalSeconds() / numRepeats; // should be ~ timerOverheadPerCall/2
}

// resolve hierarchy when the timing has ended and before outputting any statistics
void Timers::resolveHirearchy() const {
    if (hierarchyOk)
        return;
    
    for (size_t i=0; i < nodes.size(); ++i) {
        size_t ii = i;
        while (nodes[ii].parent >= 0) {
            ii = nodes[ii].parent;
            nodes[ii].childCount += nodes[i].count;
        }
    }
    hierarchyOk = true;
}

void Timers::printReport(std::ostream& out, bool colored) const {
    // first arrange nodes in a printable order
    
    // get all nodes with no parents to form roots
    std::vector<int> printStack;
    std::vector<char> printTreeConnectors;
    printStack.reserve(nodes.size());
    printTreeConnectors.reserve(nodes.size());
    for (size_t i = 0; i < nodes.size(); ++i) {
        if (nodes[i].parent < 0) {
            if ((nodes[i].name.length() > 0) && (nodes[i].count > 0)) {
                printStack.push_back(i);
                printTreeConnectors.push_back('-');
            } else {
                // a root node but it is empty; empty nodes are not shown at this stage
            }
        }
    }
    
    if (printStack.size() == 0) {
        if (colored) out << TermStyle::Underlined();
        out << "Cannot print timer hierarchy, root timer not found.";
        if (colored) out << TermStyle();
        out << std::endl;
        return;
    }
        
    // if no nodes exist or times for the root nodes are zero, skip printing
    float rootTimes = 0;
    for (auto rooti : printStack) {
        rootTimes += nodes[rooti].timer.lastSeconds();
    }
    if (rootTimes <= 0) {
        if (colored) out << TermStyle::Underlined();
        out << "Skipping timer hierarchy print, root timers were unused.";
        if (colored) out << TermStyle();
        out << std::endl;
        return;
    }
    
    if (timerOverheadPerCall == 0) {
        // dry run:
        estimateTimerOverhead(10);
        // make measurement
        estimateTimerOverhead();
    }
    
    if (!hierarchyOk)
        resolveHirearchy();
    
    // stpre base properties of cout
    auto basefmt = out.flags();
    auto basePrecision = out.precision();
    
    // specify individual timer widths:
    static const int w_hierarchy = 10;
    static const int w_name = 30;
    static const int w_totalTime = 14;
    static const int w_overheadTime = 10;
    static const int w_perentage = 7;
    static const int w_numRepeats = 15;
    static const int w_extra = 13;
    static const int w_callsPerSecond = 13;
    
    // calculate total seconds, estimated overheads and print stats
    if (colored)
        out << TermStyle::Underlined();
    out << std::setw(w_hierarchy) << (w_hierarchy > 9 ? "hierarchy " : "hier. ")
        << std::setw(w_name) << std::left << "block name" 
        << std::setw(w_totalTime) << std::right << "time [s]" 
        << std::setw(w_perentage) << "[%]"
        << std::setw(w_overheadTime) << "overhead" 
        << std::setw(w_numRepeats) << "# repeats"
        << std::setw(w_extra) << "extra/call"
        << std::setw(w_callsPerSecond) << "calls/s"
        << "\n";
    if (colored)
        out << TermStyle();
    
    // print in hierarchical order
    
        
    // now loop over the roots (and freshly inserted branches) and print them out
    for (size_t ii = 0; ii < printStack.size(); ++ii) {
        int i = printStack[ii];
        char branchType = printTreeConnectors[ii];
        bool hasChildren = false;
        
        // insert all branches from this root (insert them to indexes from ii+1 onwards)
        if (nodes[i].children.size() > 0) {
            size_t lastInsert = ii;
            for (const int j : nodes[i].children) {
                if ((nodes[j].name.length() > 0) && (nodes[j].count > 0)) {
                    ++lastInsert;
                    printStack.insert(printStack.begin() + lastInsert, j);
                    printTreeConnectors.insert(printTreeConnectors.begin() + lastInsert, '>');
                }
            }
            if (lastInsert > ii) {
                branchType = printTreeConnectors[lastInsert] = 'L';
                hasChildren = true;
            }
        }
        
        // Note that total time excludes about half of the overhead
        double ts = nodes[i].timer.totalSeconds();
        double ts_0 = nodes[0].timer.totalSeconds();
        // Overhead equals the total overhead of all timer calls made by the children plus the 'half' overhead times the number of timer calls made by itself
        double timerOverhead = nodes[i].childCount*timerOverheadPerCall + abs(nodes[i].count) * timerInaccuracy;
        if (timerOverhead > ts-timerInaccuracy)
            timerOverhead = ts-timerInaccuracy;
        {
            // hierarchy (ascii art tree)
            std::ostringstream enumName;
            // branch root offset
            for (int wi = 0; wi < nodes[i].hierarchyDepth; ++wi)
                enumName << ' ';
            // type of branch (do not branch, branch once, branch multiple times)
            enumName << (nodes[i].parent < 0 ? "─" : branchType == 'L' ? "└" : "├");
            // prepare to be the root for sub-branches
            if (hasChildren > 0)
                enumName << "┬";
            else 
                enumName << "─";
            // plot straight line
            for (int wi = 1; wi < w_hierarchy-2-nodes[i].hierarchyDepth; ++wi)
                enumName << "─";
            enumName << ' ';
            // output the generated ascii art tree
            out << std::setw(w_hierarchy) << std::left << enumName.str();
            
            // name and total time
            out << std::setw(w_name-1) << nodes[i].name << std::right << " " << std::setw(w_totalTime) 
                << std::setprecision(std::min(w_totalTime, 6)) << std::fixed << ts << " ";
                
            // time in percents
            double tsp = ts < ts_0 ? (ts * 100.0/ ts_0) : 100.0;
            out << " " << std::setw(w_perentage-1) << std::fixed << std::setprecision(1) << tsp;
                
            // timer overhead (appropriatelly colored if coloring is enabled)
            float timersBad = std::min(1.0, timerOverhead / ts);
            if (colored) out << FancyColor(1, 1-timersBad, 1-timersBad);
            out << std::setw(w_overheadTime-1) << std::fixed << std::setprecision(2) << timerOverhead;
            out.flags(basefmt); 
            out << std::setprecision(basePrecision);
            if (colored) out << TermStyle();
            
            // number of calls to the timed part of the code
            out << " " << std::setw(w_numRepeats-1) << abs(nodes[i].count);

            // extra
            out << " " << std::setw(w_extra-1) << nodes[i].extra / nodes[i].count;
            
            // calls per second (overhead is not counted in)
            out << " " << std::setw(w_callsPerSecond-1) << std::fixed << std::setprecision(0) << abs(nodes[i].count)/(ts-timerOverhead);
            out << "\n";
        }
    }
}

double Timers::seconds(size_t i, bool minusTimer) const {
    return (minusTimer ? 
        nodes[i].timer.totalSeconds() - nodes[i].childCount*timerOverheadPerCall + abs(nodes[i].count) * timerInaccuracy : 
        nodes[i].timer.totalSeconds());
}
