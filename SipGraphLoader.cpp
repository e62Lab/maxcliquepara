#include "SipGraphLoader.h"
#include <fstream>
#include <regex>
#include <iostream>
#include <cassert>


SipGraphLoader::SipGraphLoader() {
    
}

SipGraphLoader::~SipGraphLoader() {
    
}

bool SipGraphLoader::load(const char* fname) {
    std::ifstream file(fname);
    error = "";

    if (!file) {
        error = "ifstream invalid - file cannot be opened for reading";
        return false;
    }
    
    int lineNumber = 0;
    std::string line;
    try {
        std::regex ws_re("\\s+");
        while (file && error.empty()) {
            std::getline(file, line);
            
            // the following checks should never trigger on a correctly stored file
            if (line.empty())
                break;
                
            if (lineNumber < 1) {
                // read the number of vertices and only that number (nothing else can appear in this line)
                std::vector<std::string> result { std::sregex_token_iterator(line.begin(), line.end(), ws_re, -1), {} };
                if (result.size() != 1) {
                    error = "First line of file should contain only one number - the number of vertices";
                    return false;
                }
                numVertices = std::stoi(result[0]);
                // sanity check
                const int limit = 10000;
                if ((numVertices > limit) || (numVertices < 1)) {
                    error == "Number of vertices is too large ("+std::to_string(numVertices)+"), this library has a limit of "+std::to_string(limit);
                } else 
                    successorMatrix.resize(numVertices);
            } else { // lineNumber is 1 or more
                if (lineNumber > numVertices)
                    break;
                    
                // read a single vertex (num successor nodes, that is, nodes connected to it)
                std::vector<std::string> result { std::sregex_token_iterator(line.begin(), line.end(), ws_re, -1), {} };
                if (result.size() <= 1) {
                    // no successors
                } else {
                    // store data from this line: 
                    for (size_t s = 1; s < result.size(); ++s) {
                        int si = std::stoi(result[s]);
                        insertToMatrix(si, lineNumber-1);
                        insertToMatrix(lineNumber-1, si);
                    }
                }
            }
            ++lineNumber;
        }
        
        if (DEBUG_LOADER) {
            std::cout << "\n " << successorMatrix.size() << " vertices\n";
            if (successorMatrix.size() > 0)
            for (size_t i = 0; i < successorMatrix.size(); ++i) {
                std::cout << i << ": ";
                for (size_t  j = 0; j < successorMatrix[i].size(); ++j)
                    std::cout << successorMatrix[i][j] << " ";
                std::cout << ";\n";
            }
        }        
    } catch (std::invalid_argument& e) {
        error = "Invalid argument provided to stoi in line " + std::to_string(lineNumber)+ ": " + line;
    }
    
    if (lineNumber <= numVertices) {
        error = "The number of lines read is too small: " + std::to_string(lineNumber);
        return false;
    } else 
        return true;
}

unsigned int SipGraphLoader::getNumVertices() const {
    return numVertices;
}

unsigned int SipGraphLoader::getMaxVertiexIndex() const {
    return numVertices-1;
}

unsigned int SipGraphLoader::getNumEdges() const {
    unsigned int s = 0;
    for (size_t i = 0; i < successorMatrix.size(); ++i) {
        s += successorMatrix[i].size();
    }
    return s/2;
}

std::vector<std::vector<char>> SipGraphLoader::getAdjacencyMatrix() const {
    std::vector<std::vector<char>> adjacency(successorMatrix.size());
    for (size_t i = 0; i < successorMatrix.size(); ++i) {
        adjacency[i].resize(successorMatrix.size(), false);
        for (size_t j = 0; j < successorMatrix[i].size(); ++j) {
            size_t i2 = successorMatrix[i][j];
            adjacency[i][i2] = true;
        }
    }
    return adjacency;
}

std::vector<int> SipGraphLoader::getDegrees() const {
    std::vector<int> degrees(successorMatrix.size(), 0);
    for (size_t i = 0; i < successorMatrix.size(); ++i) {
        degrees[i] = successorMatrix[i].size();
    }
    return degrees;
}

void SipGraphLoader::insertToMatrix(unsigned int i1, unsigned int i2) {
    assert(i1 < successorMatrix.size());
    assert(i2 < successorMatrix.size());
    
    int insertPos = 0;
    for (size_t i = 0; i < successorMatrix[i1].size(); ++i) {
        if (successorMatrix[i1][i] < i2) {
            insertPos = i+1;
        } else if (successorMatrix[i1][i] == i2) {
            insertPos = -1;
            break;
        } else
            break;
    }
    if (insertPos != -1)
        successorMatrix[i1].insert(successorMatrix[i1].begin()+insertPos, i2);
}

