#ifndef DAT_GRAPH_LOADER_H
#define DAT_GRAPH_LOADER_H


/**
 * This file contains the class for loading .dat problem graphs, as used by BinBiq (http://biqbin.fis.unm.si/)
 * 
 * File format is as follows: 
 *      Each graph is described in a text file. If the graph has n vertices an m edges, then the file has m+1 lines (plus possibly an empty line):
 *       - The first line gives the numbers n and m.
 *       - The next m lines give edges in form of n_1 n_2 weight, where n_1 and n_2 are node indices (1-based counting) and weight is the edge weight
 * Note: currently edge weights are discarded
 */


#include <utility>
#include <vector>
#include <string>
#include "GraphLoader.h"


/**
 * @class DatGraphLoader
 * @author Matjaž
 * @date 19/03/19
 * @file DatGraphLoader.h
 * @brief 
 */
class DatGraphLoader : public GraphLoader {
public:
    bool DEBUG_LOADER = false;
    
protected:
    std::string error;
    int numVertices = 0;
    int numEdges = 0;
    std::vector<std::vector<unsigned int>> successorMatrix;
    
public:
    DatGraphLoader();
    ~DatGraphLoader();
    
    /**
     * @brief Load a .dat file
     * @param fname
     * @return true on success
     */
    virtual bool load(const char* fname) override;
    unsigned int getNumVertices() const override;
    unsigned int getMaxVertiexIndex() const override;
    unsigned int getNumEdges() const override;
    std::vector<std::vector<char> > getAdjacencyMatrix() const override;
    std::vector<int> getDegrees() const override;
	const std::string& getError() const override {return error;}
    bool verticesAreMappedFrom1based() const override {return true;}
    
protected:
    /**
     * Call this to insert a pair of vertices to the successor matrix
     * It contains a check if the vertices are already conencted
     * */
    void insertToMatrix(unsigned int i1, unsigned int i2);
};


#endif //DAT_GRAPH_LOADER_H