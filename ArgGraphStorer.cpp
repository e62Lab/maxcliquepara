#include "ArgGraphStorer.h"

#define DEBUG_STORER false


bool ArgGraphStorer::store(const char* fname) {
    if (DEBUG_STORER) 
        std::cerr << "Storing file " << fname << "; |V|=" << adjacencyMatrix.size() << "\n";

    std::ofstream outfile(fname);
    if (!outfile) {
        error += std::string("File ")+fname+" could not be opened for writing\n";
        if (DEBUG_STORER) std::cerr << error;
        return false;
    }

    // first record is the size (num vertices)
    size_t size = adjacencyMatrix.size();
    writeWord(outfile, size);
    if (!outfile) {
        error += std::string(fname)+": error reading size\n";
        if (DEBUG_STORER) std::cerr << error;
        return false;
    }
    
    // first, write all the labels for the vertices
    auto numLabels = labels.getVertexLabels().size();
    for (unsigned r = 0 ; r < size ; ++r) {
        unsigned l = numLabels > r ? labels.getVertexLabels()[r] : 0;
        writeWord(outfile, l);
    }
    if (!outfile) {
        error += std::string(fname)+": error writing attributes\n";
        if (DEBUG_STORER) std::cerr << error;
        return false;
    }
        
    // finally, write the edges are for each vertex
    for (size_t r = 0 ; r < size ; ++r) {
        // first is the nuber of edges
        std::vector<unsigned> edges;
        edges.reserve(size);
        for (size_t i = 0; i < size; ++i) {
            if (adjacencyMatrix[r][i] == true) {
                edges.push_back(i);
            }
        } 
        writeWord(outfile, edges.size());
        if (DEBUG_STORER) std::cerr << "vertex " << r << " has " << edges.size() << " edges: ";
        if (!outfile) {
            error += std::string(fname)+": error writing edges count\n";
            return false;
        }

        // for each vertex edge, the other vertex index follows
        if (labels.getEdgeLabels().size() != size) {
            error += std::string("Error, vector of edge labels is too short\n");
            if (DEBUG_STORER) std::cerr << error;
            return false;
        }
        for (int c = 0 ; c < edges.size(); ++c) {
            unsigned e = edges[c];
            writeWord(outfile, e);
            if (DEBUG_STORER) std::cerr << e << ", ";
            writeWord(outfile, labels.getEdgeLabels()[r][e]);
        }
        if (DEBUG_STORER) std::cerr << "\n";
    }

    return true;
}
