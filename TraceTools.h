#ifndef TRACE_TOOLS_H
#define TRACE_TOOLS_H

#define TRACE_MASK_BASE         0x01
#define TRACE_MASK_THREAD       0x10
#define TRACE_MASK_CLIQUE       0x20
#define TRACE_MASK_INITIAL      0x40

#ifdef NDEBUG
    #define TRACE(text, mask, level) {}
    #define TRACEVAR(var, mask, level) {}
    #define TRACEStr(str, mask, level) {}
#else
    #define IF_TRACABLE(mask, level) if ((level <= TRACE_LEVEL) && (((TRACE_MASK) & (mask)) > 0))
    #define TRACE(text, mask, level) {IF_TRACABLE((mask), level) std::cout << skipPath(__FILE__) << " line " << __LINE__ << ": " << text << "\n";}
    #define TRACEVAR(var, mask, level) {IF_TRACABLE((mask), level) std::cout << skipPath(__FILE__) << " line " << __LINE__ << ": " << #var << "=" << (var) << "\n";}
#endif

#endif //TRACE_TOOLS_H