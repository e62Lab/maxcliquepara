#ifndef MAXIMUMCLIQUEBASE_H
#define MAXIMUMCLIQUEBASE_H


#include "TraceTools.h"

#ifndef TRACE_LEVEL
    #define TRACE_LEVEL         0
#endif
#ifndef TRACE_MASK
    #define TRACE_MASK          0
#endif


// use ARGUMENT_UNUSED as an attribute in a function, when not using the declared argument to stop compiler issuing warnings
#ifdef __GNUG__ // defined by the GCC c++
    #define ARGUMENT_UNUSED __attribute__((unused))
#else
    #define ARGUMENT_UNUSED 
#endif

#include <vector>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <string>
#include <utility>
#include <bitset>
#include <map>
#include "KillTimer.h"


#include "Timers.h"

// Use macro for timeing so that it can be disabled and its overhead fixed to 0
#ifdef NO_TIMING
    struct noop { noop(const PrecisionTimer& tm) {}, noop(const PrecisionTimer&, int, long) {} };
    #define SCOPE_TIMER noop
    #define TIMER_EXTRA noop
#else
    #define SCOPE_TIMER ScopeTimer
    #define TIMER_EXTRA(tm, i, l) {tm.addExtra(i, l);}
#endif
    

/**
 * @class MaxCliqueSoftParameters
 * @author Matjaž
 * @date 23/05/17
 * @file MaximumCliqueBase.h
 * @brief This structure can be used to pass some algorithm-specific parameters to the maximum clique solver.
 * Parameters are given in form of key, value pairs of strings.
 */
typedef std::multimap<std::string, std::string> MaxCliqueSoftParameters;


template<class CharPtr>
CharPtr skipPath(CharPtr text) {
    CharPtr lastSlash = text, t = text;
    while (*t != 0) {
        if (*t == '\\' || *t == '/') lastSlash = t+1;
        ++t; 
    }
    return lastSlash;
}

template<class Ostream, class T, class U>
Ostream& operator<< (Ostream& out, const std::pair<T,U>& p) {
	out << "(" << p.first << "," << p.second << ")";
    return out;
}

template<class Ostream, class T>
Ostream& operator<< (Ostream& out, const std::vector<T>& vec) {
    if (vec.size() > 0) {
        out << "[" << vec[0];
        for (size_t i = 1; i < vec.size(); ++i)
            out << "," << vec[i]; 
        out << "]";
    } else 
        out << "[/]";
    return out;
}


class MaximumCliqueBase {
public:
    typedef std::vector<int> VertexList;
    typedef std::vector<std::vector<int> > VertexMatrix;
    
protected:
    std::string algorithmName;
    VertexList degree;                  // degree of vertices
    std::vector<std::vector<int> > A;   // 0/1 adjacency matrix
    int n;                              // n vertices
    long int countExpands;              // number of decisions
    unsigned int maxSize;               // size of max clique
    std::vector<int> solution;          // as it says
    Timers timers;                      // timers to be placed around the code
    
    // timer enums
    enum {
        timer_total,
        timer_init,
        timer_search,
        timer_initial_sort,
        timer_color_sort,
        timer_impl1,
        timer_impl2,
        timer_impl3,
        timer_invalid,
        timer_count = timer_invalid
    };

public:
    MaximumCliqueBase();
    MaximumCliqueBase(int n, const std::vector<std::vector<int> >& A, const std::vector<int>& degree);
    void search();
    const std::vector<int>& getClique() const {return solution;}
    void outputStatistics(std::ostream& out, bool colored = true);
    /**
     * @brief Gain access to the timers of the max clique solver; ususally in order to add a timer or two for pre- or post-processing
     * @return 
     */
    Timers& getTimers() {return timers;}
    static constexpr int getNumTimers() {return timer_count;}
    
protected:
    void expand(VertexList& C, VertexList& P);
    void saveSolution(const VertexList& C);
};

template<class T>
struct ClassName {
    static const char* getValue() {
        const std::type_info& tp = typeid(T());
        static std::string retVal;
        retVal = std::string(">") + tp.name() + "<";
        return retVal.c_str();
    }
};

#define REGISTER_CLASS_NAME(CLASS, NAME) template<> struct ClassName<CLASS> {static const char* getValue() {return NAME;} };
#define REGISTER_TEMPLATE1_CLASS_NAME(CLASS, NAME) template<class T> struct ClassName<CLASS<T>> {static const char* getValue() {return NAME;} };
#define REGISTER_TEMPLATE2_CLASS_NAME(CLASS, NAME) template<class T1, class T2> struct ClassName<CLASS<T1,T2>> {static const char* getValue() {return NAME;} };
#define REGISTER_TEMPLATE_EXT_CLASS_NAME(CLASS, NAME) template<class T> struct ClassName<CLASS<T>> {static const char* getValue() { \
    static std::unique_ptr<std::string> rs(new std::string); \
    if (rs->empty()) { std::stringstream s; s << NAME << "<" << ClassName<T>::getValue() << ">"; *rs = s.str(); } return rs->c_str(); } };
    
/**
 * @class MaximumCliqueProblemCommons
 * @author Matjaž
 * @date 23/03/19
 * @file MaximumCliqueBase.h
 * @brief This structure contains datatype-independent members to be used in extended classes
 */
struct MaximumCliqueProblemCommons {
    long maxAllowedSearchTime = 10; // in seconds
    KillTimer killTimer;
    
    // timer enums
    enum {
        timer_total,
        timer_init,
        timer_search,
        timer_initial_sort,
        timer_clique_estimate,
        timer_clique_estimate_impl1,
        timer_clique_estimate_impl2,
        timer_clique_estimate_impl3,
        timer_clique_estimate_impl4,
        timer_clique_estimate_impl5,
        timer_clique_estimate_impl6,
        timer_clique_estimate_impl7,
        timer_clique_estimate_impl8,
        timer_clique_estimate_impl9,
        timer_color_repair,
        timer_intersect_neighbours,
        timer_job_wait,
        timer_job_aquiring,
        timer_initial_job_wait,
        timer_job_management,
        timer_thread_management,
        timer_invalid,
        timer_count = timer_invalid
    };
    
    static void setTimers(Timers& timers){
        timers.set(timer_total, "total maximum clique");
        timers.set(timer_init, "initialization & finish", timer_total);
        timers.set(timer_search, "clique search", timer_total);
        timers.set(timer_initial_sort, "initial sorting", timer_init);
        timers.set(timer_intersect_neighbours, "intersection between two sets", timer_search);
        timers.set(timer_clique_estimate, "estimating bounds (coloring)", timer_search);
        timers.set(timer_color_repair, "color repair", timer_clique_estimate);
        timers.set(timer_job_wait, "waiting for job", timer_search);
        timers.set(timer_initial_job_wait, "initial waiting for job", timer_search);
        timers.set(timer_job_management, "job management", timer_search);
        timers.set(timer_thread_management, "thread management", timer_total);
        timers.set(timer_job_aquiring, "acquiring jobs", timer_job_wait);
    };
    
};

/**
 * @class DummyInitialSort
 * @author Matjaž
 * @date 04/08/16
 * @file MaximumCliqueBase.h
 * @brief Imlpements the function initialSort via call to the parent ColorSort policy (provided via template parameter)
 */
template<typename ColorSort>
struct DummyInitialSort : public ColorSort {
    using ColorSort::numberSort;
    //typedef typename ColorSort::GraphType GraphType;
    typedef typename ColorSort::GraphType::VertexSet VertexSet;
    //typedef typename VertexSet::VertexId VertexId;
    typedef typename ColorSort::NumberedSet NumberedSet;
    /*
    using ColorSort::init;
    using ColorSort::assignVertexNumber;
    using ColorSort::notEmpty;
    using ColorSort::topNumber;
    using ColorSort::topVertex;
    using ColorSort::popTop;
    */
    using ColorSort::initialSort;
    // void initialSort(VertexSet& c, VertexSet& vertices, NumberedSet& color) { ColorSort::initialSort(c, vertices, color); }
};
REGISTER_TEMPLATE_EXT_CLASS_NAME(DummyInitialSort, "Default");

/**
    VertexRepresentation should be
        of the most efficient scalar type on the given hardware
    VertexSetRepresentation must support:
        resize(size)
        reserve(max_size)
        add(value) // vector.push_back or set.operator[]
        pop(value) // takes last added element (that equals to v) from the set
        pop() // returns the value of the last added element, and also removes it from the set (only in vectors)
        uint size()
        operator[] (index)
        clear() // erase all elements
    Vector should be a template class (like std::vector) for dynamicly-sized arrays 
    
    function intersect(VertexSet, VertexId, VertexSet& result)
**/
template<
    class Vertex_t,
    class Set_t, 
    class Graph_t, 
    class Sort_t,
    template<class S> class InitialSort_t = DummyInitialSort
>
class MaximumCliqueProblem : public InitialSort_t<Sort_t>, MaximumCliqueProblemCommons {
public:
    typedef Vertex_t VertexId;
    typedef Set_t VertexSet;
    using MaximumCliqueProblemCommons::maxAllowedSearchTime;
    
protected:
    using Sort_t::notEmpty;
    using Sort_t::topNumber;
    using Sort_t::topVertex;
    using Sort_t::popTop;
    using Sort_t::numberSort;
    typedef InitialSort_t<Sort_t> InitialSorter;
    using InitialSorter::initialSort;
    typedef typename Sort_t::NumberedSet NumberedSet;
        
    std::string algorithmName;
    Graph_t* graph;
    VertexId n;                         // number of vertices
    unsigned int maxSize;               // size of max clique
    VertexSet maxClique;                // maxClique mapped to internal vertex numbering
    Timers timers;
    //SearchSpaceStats ssStats;
    
public:
    VertexSet knownC;

    MaximumCliqueProblem()
        : graph(nullptr), n(0), maxSize(0)
    {   
        setTimers(timers);
    }
    
    MaximumCliqueProblem(Graph_t& graph)
        : graph(&graph), n(graph.getNumVertices()), maxSize(0)
    {   
        setTimers(timers);
    }
    
    ~MaximumCliqueProblem() {
        killTimer.cancel();
    }
    
    void setGraph(Graph_t& g) {
        graph = &g;
        n = g.getNumVertices();
    }

    const Graph_t& getGraph() {
        return *graph;
    }
    
    void setParameters(const MaxCliqueSoftParameters& params) {
        // pass the parameters to all template parameter classes
    }
    
    // get the result of the search - maximal clique of the provided graph
    VertexSet getClique() const {
        VertexSet cache = maxClique;
        graph->remap(cache);
        return cache;
    }
        
    // output statistics of the last search (mostly timer readings); colored=true produces colored text on terminals supporting ANSI escape sequences
    void outputStatistics(std::ostream& out, bool colored = true) {
        std::ostringstream algorithmName;
        auto basefmt = std::cerr.flags();
        auto baseFill = std::cerr.fill();
        algorithmName << "MC(" << ClassName<VertexId>::getValue() << ","
            << ClassName<VertexSet>::getValue() << "," << ClassName<Graph_t>::getValue() << "," << ClassName<InitialSorter>::getValue() << ") ";
        std::cerr << "-- " << std::setw(30) << std::setfill('-') << std::left << algorithmName.str();
        std::cerr.flags(basefmt);
        std::cerr << std::setfill(baseFill) << "\n";
        timers.printReport(out, colored);
        
        VertexSet c = getClique();
        if (graph->wasRemapedTo0based)
            graph->remap0basedTo1based(c);
            
        if (wasSearchInterrupted()) std::cerr << "Warning, search has been interrupted, the results might not be correct\n";
        std::cerr << "Clique (" << getClique().size() << "): " << c;
    }
    
    float getSearchTime() const {
        return timers.seconds(0);
    }
    
    Timers& getTimers() {return timers;}
    
    const Timers& getTimers() const {return timers;}
    
    
    
    // run the search for max clique
    void search() {
        killTimer.start(this->maxAllowedSearchTime);
        timers.reset();
        SCOPE_TIMER tm(timers[timer_total]);
        VertexSet c; // clique
        VertexSet p; // working set of vertices
        NumberedSet numbers;       // ordered set of colors
        {
            SCOPE_TIMER tm1(timers[timer_init]);
            c.reserve(n);
            p.reserve(n);
            for (VertexId i = 0; i < n; i++) 
                p.add(i);
            
            TRACE("initial color sort", TRACE_MASK_BASE, 1);
            InitialSorter::init(graph, timers);
            initialSort(c, p, numbers);
            // some initial sorts (e.g. MCR) also find a clique
            maxSize = c.size();
            if (maxSize > 0) {
                saveSolution(c);
                c.clear();
            }
        }
        if (numbers.size() == 0) {
            SCOPE_TIMER tmc(timers[timer_clique_estimate]);
            numberSort(c, p, numbers, maxSize);
        }
        {
            SCOPE_TIMER tm2(timers[timer_search]);
            TRACE("initial expand", TRACE_MASK_BASE, 1);
            expand(c, p, numbers);        
        }
        killTimer.cancel();
        {
            // order vertex reordering back to original order (will only execute if vertices were reordered in the initialization phase)
            SCOPE_TIMER tm1(timers[timer_init]);
            graph->remap(maxClique);
            graph->orderVertices();
        }
    }
    
    bool wasSearchInterrupted() const {return killTimer.timedOut;}
    
    /**
     * @brief Test weather the supplied set of vertices is a clique
     * @param c the input vertex set (eleged clique)
     * @return true if c is clique
     */
    bool testClique(const VertexSet& c) const {
        for (auto v1 : c) {
            for (auto v2 : c) {
                if ((v2 != v1) && (!graph->adjacencyMatrix[v1][v2]))
                    return false;
            }
        }
        return true;
    }

    void outputCliqueEdges(const VertexSet& c) const {
        for (auto v1 : c) {
            std::cerr << v1 << ": ";
            for (auto v2 : c) {
                if ((v2 != v1) && (graph->adjacencyMatrix[v1][v2]))
                    std::cerr << v2 << ", ";
            }
            std::cerr << "\n";
        }
    }
    
    Sort_t& getSort() {return *this;}
    InitialSorter& getInitialSort() {return *this;}
    
protected:   
    // main recursive function 
    //  c ... clique candidate, 
    //  p ... set of candidate vertices that can be added to c,
    //  numbered ... an ordered and numbered set of vertices
    void expand(VertexSet& c, VertexSet& p, NumberedSet& np) {
        while (notEmpty(np)) {
            if (c.size() + topNumber(np) <= maxSize || killTimer.timedOut) {return;}
            auto v = topVertex(np, p);
            TRACEVAR(v, TRACE_MASK_BASE, 2);
            //std::cout << "v" << v << " ";
            c.add(v);
            VertexSet p1;
            {
                SCOPE_TIMER tm(timers[timer_intersect_neighbours]);
                graph->intersectWithNeighbours(v, p, p1);
            }
            /*
            bool b = false;
            if ((v==134 && c.isIntersectionOf(c)) || (c.isIntersectionOf(knownC) && maxSize < knownC.size())) {
                std::cout << c << ":\n";
                std::cout << p.size() << "→" << p1.size() << "/" << maxSize << " " << p << "→" << p1 << "\n";
                b = true;
            }
            */
            
            if (p1.size() == 0) {
                if (c.size() > maxSize) 
                    saveSolution(c);
            } else {
                TRACE("Coloring", TRACE_MASK_BASE, 2);
                NumberedSet np1;
                {
                    SCOPE_TIMER tm(timers[timer_clique_estimate]);
                    TIMER_EXTRA(timers, timer_clique_estimate, p1.size());
                    numberSort(c, p1, np1, maxSize);
                }
                TRACEVAR(c, TRACE_MASK_BASE, 3);
                TRACEVAR(p1, TRACE_MASK_BASE, 3);
                TRACE("Expanding", TRACE_MASK_BASE, 2);
                expand(c, p1, np1);
            }
            
            c.remove(v);
            popTop(np, p);
            //std::cout << "p" << np.size() << " ";
        }
    }
    
    // when a clique, larger than its predecessor is found, call this function to store it
    void saveSolution(const VertexSet& c) {
        maxSize = c.size();
        maxClique = c;
        //std::cout << "new clique " << maxSize << "\n";
    }
};

template<class T>
struct isTypeASet {
    enum {value = false};
};

REGISTER_CLASS_NAME(long int, "long int");
REGISTER_CLASS_NAME(int, "int");
REGISTER_CLASS_NAME(short int, "short int");
REGISTER_CLASS_NAME(char, "char");

#endif // MAXIMUMCLIQUEBASE_H
