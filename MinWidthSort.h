#ifndef HEADER_AC0AEB905A1ABA15
#define HEADER_AC0AEB905A1ABA15


#include "MaximumCliqueBase.h"


template<class ColorSort>
struct MinWidthSort : ColorSort {
    using ColorSort::numberSort;
    typedef typename ColorSort::GraphType GraphType;
    typedef typename GraphType::VertexSet VertexSet;
    typedef typename ColorSort::NumberedSet NumberedSet;
    
    using ColorSort::init;
    
    void initialSort(VertexSet& c, VertexSet& vertices, NumberedSet& color) {
        struct Vertex {
            int degree;
            int index;
        };
        
        size_t n = vertices.size();
        std::vector<Vertex> l(n);
        for (size_t i = 0; i < n; ++i) {
            l[i].index = vertices[i];
            l[i].degree = this->graph->degrees[vertices[i]];
        }
            
        int k = l.size() - 1;
        while (l.size() > 0) {
            size_t i = 0;
            for (size_t j = 1; j < l.size(); ++j) 
                if (l[j].degree < l[i].degree) i = j;
            vertices[k] = l[i].index;
            l.erase(l.begin()+i);
            for (auto& u : l) 
                if (this->graph->adjacencyMatrix[u.index][vertices[k]] == 1)
                    u.degree--;
            --k;
        }
    }
};
REGISTER_TEMPLATE1_CLASS_NAME(MinWidthSort, "Min width sort");


#endif // header guard 
