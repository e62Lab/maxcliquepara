#ifndef HEADER_F7D1CB85B2C4397
#define HEADER_F7D1CB85B2C4397


#include "MaximumCliqueBase.h"


template<class ColorSort>
struct ReNumberSort : ColorSort {
    typedef typename ColorSort::GraphType GraphType;
    typedef typename GraphType::VertexSet VertexSet;
    typedef typename ColorSort::NumberedSet NumberedSet;
    typedef typename GraphType::VertexId VertexId;
    
    using ColorSort::init;
    
    // take clique c and candidate vertex set p as input
    // return numbered vertex set np as output
    void numberSort(const VertexSet& c, VertexSet& p, NumberedSet& np, unsigned int maxSize = 0) {
        // start with regular greedy coloring
        size_t m = p.size();
        size_t numbers = 0;
        auto threshold = maxSize - c.size();
        
        for (size_t i = 0; i < m; i++)
            this->colorSet[i].clear();
                
        for (size_t i = 0; i < m; i++) {
            //this->colorSet[i].clear();
            VertexId v = p[i];
            size_t k = 0;
            
            while (intersectionExists(v, this->colorSet[k])) 
                k++;
                
            this->colorSet[k].add(v);
            
            // apply recolor to vertex if its color is high enough
            if ((k >= threshold) && (k == numbers)) {
                renumber(v, k, threshold);
            }
            
            numbers = std::max(numbers, k+1);
        }
        
        // copy colors to vector
        np.resize(m);
        for (size_t k = 0, i = 0; k < numbers; k++) {
            for (size_t j = 0; j < this->colorSet[k].size(); j++) {
                assignVertexNumber(p, np, i++, this->colorSet[k][j], k+1);
            }
        }
    }
    
    void renumber(VertexId p, VertexId no, VertexId threshold) {
        ScopeTimer tm((*this->timers)[MaximumCliqueProblemCommons::timer_color_repair]);
        auto& pAdjecent = this->graph->adjacencyMatrix[p];
        threshold = std::min(threshold, no);
        //std::cout << "renumber " << p << "[" << no << "]";
        for (VertexId k1 = 0; k1 < threshold-1; ++k1) {
            // find a vertex from color[k1], that is the only neighbour of p in color[k1]
            auto& color1 = this->colorSet[k1];
            VertexId q = 0;
            size_t qi = 0;
            size_t numAdjacent = 0;
            for (size_t q2i = 0; (q2i < color1.size()) && (numAdjacent < 2); ++q2i) {
                auto q2 = color1[q2i];
                if (pAdjecent[q2]) {
                    ++numAdjacent;
                    q = q2;
                    qi = q2i;
                }
            }
            if (numAdjacent == 1) {
                for (VertexId k2 = k1+1; k2 < threshold; ++k2) {
                    if (!intersectionExists(q, this->colorSet[k2])) {
                        // 3-way swap colors of p, q
                        //std::cout << " to [" << k1 << "], moved " << q << " to [" << k2 << "]";
                        this->colorSet[no].remove(p);
                        color1[qi] = p;
                        this->colorSet[k2].add(q);
                        //std::cout << "\n";
                        return;
                    }
                }
                // only do this for one vertex → break the loop
                break;
            }
        }
        //std::cout << "\n";
    }
};
REGISTER_TEMPLATE_EXT_CLASS_NAME(ReNumberSort, "Re-number sort");



#endif // header guard 
