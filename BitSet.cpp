#include "BitSet.h"

std::ostream& operator<< (std::ostream& out, const BitSet& b) {
    if (b.size() == 0) {
        std::cout << "[/]";
    } else {
        std::cout << "[";
        bool comma = false;
        for (size_t i = 0; i < b.size(); ++ i) {
            if (b[i]) {
                if (comma)
                    std::cout << ",";
                comma = true;
                std::cout << i;  
            }
        }
        std::cout << "]";
    }
    return out;
}
