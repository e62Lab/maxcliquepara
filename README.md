# MaxCliquePara

## About

This is the extended code for solving maximum clique problem in parallel (multi-threaded). 
Features:
- Can read input graph files of type:
	* Dimacs text ([http://lcs.ios.ac.cn/~caisw/Resource/about_DIMACS_graph_format.txt])
	* Arg library type ([http://mivia.unisa.it/datasets/graph-database/arg-database/])
	* SIP problem type ([http://liris.cnrs.fr/csolnon/SIP.html ])
	* BiqBin type ([http://biqbin.fis.unm.si/])
- Can invert the graph edges before searching for clique - translating the max clique problem to max independent set problem
- Can performe precise timing on individual parts of the algorithm (define NO_TIMING in main or through compile time command to disable timing and increase performance)
- Supports multiple algorithms for coloring, sorting vertices, etc, and multiple types of collections to hold vertices (bit-stream, vector, ...).

## How to use it

Assuming the OS is Linux, the default implementation can be used through the command line:
	
	make
	./maxClique -input ~/Downloads/keller4_clq.dat -threads 111 -invertInput

Compiling and command line have not been tested on Windows yet.

