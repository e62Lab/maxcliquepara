#ifndef SIP_GRAPH_LOADER_H
#define SIP_GRAPH_LOADER_H


/**
 * This file contains the class for loading SIP problem graphs, as provided here: http://liris.cnrs.fr/csolnon/SIP.html 
 * 
 * File format is as follows: 
 *      Each graph is described in a text file. If the graph has n vertices, then the file has n+1 lines:
 *       - The first line gives the number n of vertices.
 *       - The next n lines give, for each vertex, its number of successor nodes, followed by the list of its successor nodes.
 */


#include <utility>
#include <vector>
#include <string>
#include "GraphLoader.h"


/**
 * @class SipGraphLoader
 * @author Matjaž
 * @date 03/11/16
 * @file SipGraphLoader.h
 * @brief 
 */
class SipGraphLoader : public GraphLoader {
public:
    bool DEBUG_LOADER = false;
    
protected:
    std::string error;
    int numVertices = 0;
    std::vector<std::vector<unsigned int>> successorMatrix;
    
public:
    SipGraphLoader();
    ~SipGraphLoader();
    
    /**
     * @brief Load a SIP file (not a very well defined format, even the name is format name is not specifeid, the file also has no common suffix)
     * @param fname
     * @return 
     */
    virtual bool load(const char* fname) override;
    unsigned int getNumVertices() const override;
    unsigned int getMaxVertiexIndex() const override;
    unsigned int getNumEdges() const override;
    std::vector<std::vector<char> > getAdjacencyMatrix() const override;
    std::vector<int> getDegrees() const override;
	const std::string& getError() const override {return error;}
    bool verticesAreMappedFrom1based() const override {return false;}
    
protected:
    /**
     * Call this to insert a pair of vertices to the successor matrix
     * It contains a check if the vertices are already conencted
     * */
    void insertToMatrix(unsigned int i1, unsigned int i2);
};


#endif //SIP_GRAPH_LOADER_H