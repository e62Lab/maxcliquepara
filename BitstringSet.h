#ifndef BITSTRINGSET_H
#define BITSTRINGSET_H


#include "BitSet.h"
#include "MaximumCliqueBase.h"
#include "SteadyGreedyColorSort.h" // SteadyVectorSet is reused here


// Set of vertices is based on std::bitset
// every set consists of two sets with different ordering
// used as VertexSetRepresentation in MaximumCliqueProblem
class BitstringSet : protected BitSet {
    mutable size_t countCache;
    
public:
    typedef unsigned int VertexId;
    
    BitstringSet() : countCache(0) {}
    BitstringSet(const BitstringSet& other) : BitSet(other), countCache(other.countCache) {}
    using BitSet::resize;
    void reserve(size_t s) {BitSet::resize(s);};
    void add(VertexId value) {setValue(value, true); ++countCache;}
    void remove(VertexId value) {setValue(value, false); --countCache;}
    size_t recount() const {countCache = count(); return countCache;}
    size_t size() const {return countCache;}
    size_t alsize() const {return BitSet::size();}
    bool operator[](size_t index) const {return getValue(index);}
    BoolProxy operator[](size_t index)  {return BoolProxy(this, index);}
    //using BitSet::operator[];
    void clear() {BitSet::clear(); countCache = 0;};
    using BitSet::firstSetBit;
    using BitSet::lastSetBit;
    using BitSet::nextSetBit;
    using BitSet::operator~;
    
    void operator=(const BitstringSet& other) {BitSet::operator=(other); countCache=other.countCache;}
    void operator&=(const BitSet& other) {BitSet::operator&= (other);}
    void operator&=(const BitstringSet& other) {BitSet::operator&= (other);}
    void operator^=(const BitstringSet& other) {BitSet::operator^= (other);}
    
    friend void intersectWithAdjecency (const BitstringSet& v, const BitstringSet& adj, BitstringSet& result) {
        result = v;
        result &= adj;
        result.recount();
    }
    
    void remove(const BitstringSet& values) {
        // this function only works correctly if all the specified values are set in the target bitstring
        *this ^= values;
        countCache -= values.countCache;
    }
    
    /*
     * why is this unimplemented thingy here?
    bool isIntersectionOf(const BitstringSet& bigSet) {
        return  false;
    }*/
    
    bool contains(VertexId v) const {
        return getValue(v);
    }
    
    friend void fillWithRange(BitstringSet& b, int min, int max);
    
    const BitSet toBitSet() const {
        return *this;
    }
};
REGISTER_CLASS_NAME(BitstringSet, "bitstring based set");

template<> struct isTypeASet<BitstringSet> { enum {value = true}; };

template<class Out>
Out& operator<< (Out& out, const BitstringSet& b) {
    int lastBit=0;
    if (b.size() == 0) {
        out << "[/]";
    } else {
        out << "[";
        for (int i = 0; i < b.size(); ++i) {
            if (i > 0)
                out << ",";
            lastBit = b.nextSetBit(lastBit);
            out << lastBit;
            ++lastBit;
        }
        out << "]";
    }
    return out;
}


#endif // BITSTRINGSET_H
