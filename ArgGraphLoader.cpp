#include "ArgGraphLoader.h"
#include <iostream>
#include <set>


bool ArgGraphLoader::load(const char* fname) {
    std::ifstream infile(fname);
    if (!infile) {
        error += std::string("File ")+fname+" could not be opened\n";
        return false;
    }

    // first record is the size (num vertices)
    size_t size = readWord(infile);
    if (!infile) {
        error += std::string(fname)+": error reading size\n";
        return false;
    }

    // now the attributes follow
    // a list of labels, indexed by vertex index
    vertexLabels.resize(size);
    // a matrix of edge labels, indexed by vertex
    edgeLabels.resize(size);
    for (auto& e : edgeLabels)
        e.resize(size);

    // first, read all the labels for the vertices
    for (unsigned r = 0 ; r < size ; ++r)
        vertexLabels[r] = readWord(infile);

    if (!infile) {
        error += std::string(fname)+": error reading attributes\n";
        return false;
    }
        
    // finally, read the edges for each vertex
    for (size_t r = 0 ; r < size ; ++r) {
        // first is the nuber of edges
        auto c_end = readWord(infile);
        if (DEBUG_LOADER) std::cerr << "vertex " << r << " has " << c_end << " edges: ";
        if (!infile) {
            error += std::string(fname)+": error reading edges count\n";
            return false;
        }
 
        // for each vertex edge, the other vertex index follows
        for (int c = 0 ; c < c_end ; ++c) {
            unsigned e = readWord(infile);
            if (DEBUG_LOADER) std::cerr << e << ", ";

            if (e >= size) {
                error += std::string(fname)+": edge index " + std::to_string(e) + " is out of bounds (" + std::to_string(size) + ")\n";
                return false;
            }

            edgeLabels[r][e] = readWord(infile);
        }
        if (DEBUG_LOADER) std::cerr << "\n";
    }

    infile.peek();
    if (!infile.eof())
        error += std::string(fname)+" warning: after reading the file successfully, end of file was not reached\n";

    return true;
}

unsigned int ArgGraphLoader::getNumEdges() const {
    unsigned int n = 0;
    for (auto& e : edgeLabels) {
        for (size_t j = 0; j < e.size(); ++j) {
            n += (e[j] == 0 ? 0 : 1);
        }
    }
    return n;
}

std::vector<std::vector<char> > ArgGraphLoader::getAdjacencyMatrix() const {
    std::vector<std::vector<char>> adjacency(getNumVertices());
    for (size_t i = 0; i < adjacency.size(); ++i) {
        adjacency[i].resize(getNumVertices(), false);
        for (size_t j = 0; j < adjacency[i].size(); ++j) {
            adjacency[i][j] = (edgeLabels[i][j] > 0);
        }
    }
    return adjacency;
}
    
std::vector<int> ArgGraphLoader::getDegrees() const {
    std::vector<int> degrees(getNumVertices(), 0);
    for (size_t i = 0; i < edgeLabels.size(); ++i) {
        degrees[i] = 0;
        for (size_t j = 0; j < edgeLabels[i].size(); ++j) {
            degrees[i] += (edgeLabels[i][j] == 0 ? 0 : 1);
        }
    }
    return degrees;
}

