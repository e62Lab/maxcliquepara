#ifndef HEADER_DEFAAFC421A53A4F
#define HEADER_DEFAAFC421A53A4F

#include "MaximumCliqueBase.h"
#include <thread>
#include <pthread.h>
#include <memory>
#include <deque> // TODO: test deque vs. vector for storing a queue of jobs


using std::swap;


/**
    VertexRepresentation should be
        of the most efficient scalar type on the given hardware
    VertexSetRepresentation must support:
        resize(size)
        reserve(max_size)
        add(value) // vector.push_back or set.operator[]
        pop(value) // takes last added element (that equals to v) from the set
        pop() // returns the value of the last added element, and also removes it from the set (only in vectors)
        uint size()
        operator[] (index)
        clear() // erase all elements
        TODO: list others
    Vector should be a template class (like std::vector) for dynamicly-sized arrays 
    
    function intersect(VertexSet, VertexId, VertexSet& result)
**/
template<
    class Vertex_t,
    class Set_t, 
    class Graph_t, 
    class Sort_t,
    template<class S> class InitialSort_t
>
class ParallelMaximumCliqueProblem : public MaximumCliqueProblemCommons, InitialSort_t<Sort_t> {
public:
    typedef Vertex_t VertexId;
    typedef Set_t VertexSet;
    typedef Graph_t Graph;
    typedef Sort_t Sorter;
    typedef InitialSort_t<Sort_t> InitialSorter;
    typedef typename Sorter::NumberedSet NumberedSet;
    
    struct Job {
        VertexSet c;            // starting clique
        VertexSet vertices;     // available vertices
        NumberedSet numbers;    // available vertices
        unsigned int estimatedMax;       // used to queue jobs from the most perspective down to the least perspective
        
        Job()  {}
        Job(const VertexSet& cin, const VertexSet& vin, const NumberedSet& nin, int estimate) : c(cin), vertices(vin), numbers(nin), estimatedMax(estimate)  {}
        void set(const VertexSet& cin, const VertexSet& vin, const NumberedSet& nin, int estimate) {c = cin; vertices = vin; numbers = nin; estimatedMax = estimate;}
        friend void swap(Job& a, Job& b) {
            std::swap(a.c, b.c); 
            std::swap(a.vertices, b.vertices);
            std::swap(a.numbers, b.numbers);
            std::swap(a.estimatedMax, b.estimatedMax);
        }
    };

    struct Worker : Sorter {
        // parent pMCP - the problem for which this worker is working
        ParallelMaximumCliqueProblem* parent;
        // Timers with the same  hirearchy that Timers of pMCP
        Timers timers;
        // the same graph as the parent pMCP
        Graph* graph;
        // current clique
        VertexSet c;
        // id of the worker (helps with the end-time statistics)
        int id;
        // copy of parent maxSize (size of the active maximumClique)
        unsigned int localMaxSize;
        // break the work on a job when going below the specified level (size of a clique); search will not continue on a level lower than jobLevelBreak
        unsigned int jobLevelBreak;
        
        using Sorter::notEmpty;
        using Sorter::topNumber;
        using Sorter::topVertex;
        using Sorter::popTop;
        using Sorter::numberSort;
        
        Worker() : graph(nullptr) {MaximumCliqueProblemCommons::setTimers(timers);}
        
        void setup(ParallelMaximumCliqueProblem* pmcp, int newId) {
            parent = pmcp;
            graph = pmcp->graph;
            id = newId;
            Sorter::init(graph, timers);
        }
        
        void threadFunc() {
            localMaxSize = parent->maxSize;
            TRACE("threadFunc start", TRACE_MASK_THREAD, 1);
            int jobsDone = 0;
            try {
                { // this scope is for the scope timer in the next line: the scope must end before the function sends the results to the parent thread
                    SCOPE_TIMER tm(timers[MaximumCliqueProblemCommons::timer_search]);
                    // work while there are jobs available
                    TRACE("threadFunc: getting a job", TRACE_MASK_THREAD, 1);
                    while (true) {
                        Job job;
                        // grab a job from the queue
                        {  
                            SCOPE_TIMER sc(timers[jobsDone == 0 ? MaximumCliqueProblemCommons::timer_initial_job_wait : MaximumCliqueProblemCommons::timer_job_wait]);
                            std::lock_guard<std::mutex> lk(parent->mutexJobs); 
                            SCOPE_TIMER sc1(timers[timer_job_aquiring]);
                            TRACEVAR(parent->activeJobs, TRACE_MASK_THREAD, 2);
                            // if there is no queued jobs then wait; if there is also no active jobs (that would be adding to the queue) then quit
                            if (parent->jobs.size() == 0) {
                                /*
                                // when dynamic tree splitting is used, try the following:
                                if (parent->activeJobs == 0)
                                    break;
                                else
                                    continue;
                                    */
                                break;
                            }
                            
                            // take a job from the top of the queue, check if a new job can be added to the queue immediately 
                            if ((parent->jobs.size() > parent->maxJobs) || (parent->jobs.back().estimatedMax <= localMaxSize)) {
                                // job on the queue is a small one; do not split it further (the number to check against is arbitrary)
                                // (swap is the fastest operation, job is empty now)
                                swap(job, parent->jobs.back());
                                parent->jobs.pop_back();
                                parent->requireMoreJobs = true;
                                // mark where to end execution
                                jobLevelBreak = job.c.size();
                            } else {
                                // copy the job out of the queue
                                job = parent->jobs.back();
                                // change the job in the queue to make it available for another worker
                                popTop(parent->jobs.back().numbers, parent->jobs.back().vertices);
                                parent->jobs.back().estimatedMax = topNumber(parent->jobs.back().numbers) + parent->jobs.back().c.size();
                                // mark where to end execution
                                jobLevelBreak = job.c.size()+1;
                            }
                            // mark that another job has been activated
                            ++parent->activeJobs;
                            // take the working clique from the job description (clique that is saved in the job is only used at the start)
                            c = job.c;
                        }
                        TRACE("threadFunc: calling expand", TRACE_MASK_THREAD, 1);
                        //std::cout << "new thread job " << job.vertices.size() << std::endl;
                        // work on the job
                        {
                            SCOPE_TIMER sc(timers[MaximumCliqueProblemCommons::timer_total]);
                            TRACEVAR(jobLevelBreak, TRACE_MASK_THREAD, 1);
                            // dig in
                            expand(job);
                            ++jobsDone;
                        }
                        TRACE("threadFunc: job completed", TRACE_MASK_THREAD, 1);
                        // deacivate the completed job
                        {
                            SCOPE_TIMER sc(timers[MaximumCliqueProblemCommons::timer_job_wait]);
                            std::lock_guard<std::mutex> lk(parent->mutexJobs);
                            SCOPE_TIMER sc1(timers[timer_job_aquiring]);
                            --parent->activeJobs;
                        }
                    }
                } // timer scope end           
                // send statistics to the parent
                {
                    TRACE("threadFunc: reposting stats", TRACE_MASK_THREAD, 1);
                    std::lock_guard<std::mutex> lk(parent->mutexJobs); 
                    // merge local timers with main timers; mark total_timer to be special / to not be added
                    timers.mark(MaximumCliqueProblemCommons::timer_total);
                    // this function increases parallelization overhead since it executes in each thread (which it must, since timer calls
                    //  in threads produce different overhead from timers in main process) and its execution time can not be easily subtracted
                    timers.estimateTimerOverhead(10);
                    timers.estimateTimerOverhead(100);
                    timers.resolveHirearchy();
                    parent->timers.addTimes(timers);
                    parent->workerActiveTimes.push_back(timers.seconds(MaximumCliqueProblemCommons::timer_total, true));
                }
            } catch (const char* e) {
                std::cout << "Exception in thread: " << e << "\n";
            }
        }
        
        // main recursive function (parallel)
        void expand(Job& job) {
            TRACE("expand start", TRACE_MASK_CLIQUE, 2);
            
            localMaxSize = parent->maxSize;
            TRACEVAR(localMaxSize, TRACE_MASK_CLIQUE, 2);
            while (notEmpty(job.numbers)) {
                TRACEVAR(job.numbers.size(), TRACE_MASK_CLIQUE, 2);
                if (job.estimatedMax <= localMaxSize || parent->killTimer.timedOut) {return;}
                Job newJob;
                auto v = topVertex(job.numbers, job.vertices);
                popTop(job.numbers, job.vertices);
                job.estimatedMax = c.size() + topNumber(job.numbers);
                {
                    SCOPE_TIMER tm(timers[timer_intersect_neighbours]);
                    graph->intersectWithNeighbours(v, job.vertices, newJob.vertices);
                }
                
                //newJob.c = job.c;
                c.add(v);
                if (c.size() > localMaxSize) { // condition (newJob.vertices.size() == 0) is left out to make maxSize up to date at all times
                    localMaxSize = parent->saveSolution(c);
                }
                
                TRACEVAR(newJob.vertices.size(), TRACE_MASK_CLIQUE, 2);
                if (newJob.vertices.size() > 0) {
                    // number vertices
                    {
                        SCOPE_TIMER tm(timers[timer_clique_estimate]);
                        TIMER_EXTRA(timers, timer_clique_estimate, newJob.vertices.size());
						newJob.numbers.resize(newJob.vertices.size());
                        numberSort(c, newJob.vertices, newJob.numbers, localMaxSize);
                        newJob.estimatedMax = c.size() + topNumber(newJob.numbers);
                        TRACEVAR(newJob.estimatedMax, TRACE_MASK_CLIQUE, 2);
                    }
                    
                    /*
                    // quick and dirty (not protected by mutex but should be atomic) check weather new job should be pushed to the queue
                    //bool jobQueueFull = (jobToFork == nullptr) || (!parent->requireMoreJobs);
                    if (parent->requireMoreJobs) { 
                        SCOPE_TIMER tm(timers[timer_job_management]);
                        std::lock_guard<std::mutex> lk(parent->mutexJobs); 
                        // check the number of jobs in queue again (now protected by mutex)
                        if (parent->jobs.size() < parent->maxJobs) {
                            TRACE("expand adding a job to the queue");
                            TRACEVAR(parent->jobs.size());
                            // fork the pre-prepared job (far more appropriate than forking current job)
                            parent->addJob(*jobToFork);
                            // mark the forked job as off-limits to this worker
                            jobLevelBreak = jobToFork->c.size()+1;
                            TRACEVAR(jobLevelBreak);
                            // disable further forks; better alternative would be to allow further forks on a higher level
                            // it would be harder to implement and would introduce additional overhead (unless the main recursion was transformed to iteration)
                            jobToFork = nullptr;
                        }
                    }
                    */
                    
                    // continue exploration of the search tree within this job
                    TRACE("expand calling expand", TRACE_MASK_CLIQUE, 2);
                    expand(newJob);
                    if (c.size() <= jobLevelBreak) { // <= instead of <, because c.remove(v) has not been called yet
                        TRACE("breaking worker search tree", TRACE_MASK_CLIQUE, 2);
                        TRACEVAR(jobLevelBreak, TRACE_MASK_CLIQUE, 2); 
                        return;
                    }
                }
                c.remove(v);
            }
            TRACE("expand end", TRACE_MASK_CLIQUE, 2);
        }
    };
    
protected:
    using Sort_t::notEmpty;
    using Sort_t::topNumber;
    using Sort_t::topVertex;
    using Sort_t::popTop;
    using Sort_t::numberSort;
    using InitialSorter::initialSort;

    std::string algorithmName;
    Graph* graph;
    VertexId n;                         // number of vertices
    unsigned int maxSize;               // size of max clique
    unsigned int numThreads;            // stores the number of threads used in the last search (where this number a parameter to the function)
    VertexSet maxClique;
    Timers timers;
    std::deque<Job> jobs;
    bool requireMoreJobs;
    size_t maxJobs, activeJobs;
    std::vector<double> workerActiveTimes;
    std::mutex mutexJobs, mutexQ;
    
public:
    VertexSet knownC;

    ParallelMaximumCliqueProblem(Graph& graph) : graph(&graph), n(graph.getNumVertices()), maxSize(0), activeJobs(0) {   
        setTimers(timers);
    }
    
    // get the result of the search - a copy of the maximal clique of the provided graph
    VertexSet getClique() const {
        // note that reordering is not necessary since the algorithm reorders it when it ends search
        return maxClique;
        /*
        VertexSet cache = maxClique;
        graph->remap(cache);
        return cache;
        */
    }
    
    // output statistics of the last search (mostly timer readings); colored=true produces colored text on terminals supporting ANSI escape sequences
    void outputStatistics(std::ostream& out, bool colored = true) {
        std::ostringstream algorithmName;
        auto basefmt = out.flags();
        auto baseFill = out.fill();
        algorithmName << "pMC[" << numThreads << " threads, " << maxJobs << " jobs](" << ClassName<VertexId>::getValue() << ","
            << ClassName<VertexSet>::getValue() << "," << ClassName<Graph>::getValue() << "," << ClassName<InitialSorter>::getValue() << ") ";
        out << "-- " << std::setw(80-3) << std::setfill('-') << std::left << algorithmName.str();
        out.flags(basefmt);
        out << std::setfill(baseFill) << "\n";
        timers.printReport(out, colored);
        VertexSet mc = getClique();
        
        if (wasSearchInterrupted()) out << "Warning, search has been interrupted, the results might not be correct\n";
        if (graph->wasRemapedTo0based)
            graph->remap0basedTo1based(mc);
        out << "Clique (" << getClique().size() << "): " << mc;
        out.flags(basefmt);
        out << std::setfill(baseFill);
    }
    
    double workerEfficiency() const {
        double maxTime = *std::max_element(workerActiveTimes.begin(), workerActiveTimes.end());
        double eff = 0;
        for (size_t i = 0; i < workerActiveTimes.size(); ++i) {
            eff += workerActiveTimes[i];
        }
        return eff / (maxTime*workerActiveTimes.size());
    }
    
    void sortLastJob() {
        if (jobs.size() > 1) {
            for (auto j = jobs.end()-1; (j != jobs.begin()) && ((j-1)->vertices.size() > j->vertices.size()); --j)
                swap(*j, *(j-1));
        }
    }
    
    void addJob(Job& job) {
        // LIFO queue, less copying
        jobs.push_front(Job());
        swap(jobs.front(), job);
        requireMoreJobs = jobs.size() == maxJobs;
        
        /*
        // Jobs queue is of LIFO type
        jobs.push_front(job);
        requireMoreJobs = jobs.size() == maxJobs;
        */
        
        // Job sorting, it works better without one :(
        //jobs.push_back(job);
        //sortLastJob();
    }
    
    void swapJob(Job& job) {
        if (jobs.size() > 0 && job.estimatedMax < jobs.back().estimatedMax) {
            //swap(job, jobs.back());
            //sortLastJob();
        }
        /* // DEBUG
        static int cnt = 1;
        if (cnt > 0) {
            --cnt;
            for (auto& j: jobs)
                std::cout << j.estimatedMax << " ";
            std::cout << "\n";
        }*/
    }
    
    // run the search for max clique
    void search(unsigned int numThreads, unsigned int numJobs, std::vector<int>& affinities) {
        killTimer.start(this->maxAllowedSearchTime);
        this->numThreads = numThreads;
        timers.reset();
        SCOPE_TIMER tm(timers[timer_total]);
        
        // initial sorting (ordering)
        VertexSet c; // clique
        VertexSet p; // working set of vertices
        NumberedSet numbers;       // ordered set of colors
        {
            // setting the order of vertices (initial sort that renumbers vertices in the input graph)
            SCOPE_TIMER tm1(timers[timer_init]);
            c.reserve(n);
            p.reserve(n);
            for (VertexId i = 0; i < n; i++) 
                p.add(i);
            
            InitialSorter::init(graph, timers);
            TRACE(typeid(InitialSorter).name(), TRACE_MASK_CLIQUE, 1);
            initialSort(c, p, numbers);
            
            // some initial sorts (e.g. MCR) also find a clique
            if (c.size() > 0) {
                saveSolution(c);
                c.clear();
            }
        }
        if (numbers.size() == 0) {
            // if initial sort did not setup "numbers", numberSort must be called
            SCOPE_TIMER tmc(timers[timer_clique_estimate]);
            numberSort(c, p, numbers, maxSize);
        }
        
        maxJobs = std::min(numJobs, numThreads);
//        jobs.reserve(maxJobs+1);
        // vetrex set c is empty at this point
        jobs.push_back(Job(c, p, numbers, c.size() + topNumber(numbers)));
        activeJobs = 0;
        requireMoreJobs = jobs.size() < maxJobs;
        
        // Create threads and workers
        std::vector<std::unique_ptr<std::thread> > threads;
        std::vector<Worker> workers;
        threads.resize(numThreads);
        workers.resize(numThreads);
        
        // associate thread & worker pairs (this can take several hundred microseconds), but lock jobs first since initial sorting has not been done
        //std::lock_guard<std::mutex> lk(mutexJobs);
        {
            SCOPE_TIMER tm1(timers[timer_thread_management]);
            for (unsigned int i = 0; i < numThreads; ++i) {
                workers[i].setup(this, i);
                threads[i] = std::unique_ptr<std::thread>(new std::thread([&workers, i](){workers[i].threadFunc();}));
                if (affinities.size() > 0) {
                    // GCC on LINUX specific code (actually pthread specific)
                    // since GCC implements c++ threads using pthread library, we can use pthread ability to assign affinity
                    cpu_set_t cpuset;
                    CPU_ZERO(&cpuset);
                    CPU_SET(affinities[i % affinities.size()], &cpuset);
                    pthread_setaffinity_np(threads[i]->native_handle(), sizeof(cpu_set_t), &cpuset);
                }
            }
        }
        TRACE("Done building threads, waiting for join", TRACE_MASK_THREAD, 1);
        // wait for all the workers to finish
        for (unsigned int i = 0; i < numThreads; ++i) {
            threads[i]->join();
            TRACE("Thread joined to main thread", TRACE_MASK_THREAD, 1);
        }
        killTimer.cancel();
        jobs.clear();
        
        {
            // order vertex reordering back to original order (will only execute if vertices were reordered in the initialization phase)
            SCOPE_TIMER tm1(timers[timer_init]);
            graph->remap(maxClique);
            graph->orderVertices();
            VertexSet v = maxClique;
        }
        TRACE("search: end", TRACE_MASK_THREAD, 1)
    }
    
    const Timers& getTimers() const {return timers;}
    
    bool wasSearchInterrupted() const {return killTimer.timedOut;}
    
protected:
    // when a clique, larger than its predecessor is found, call this function to store it
    unsigned int saveSolution(const VertexSet& c) {
        unsigned int ret;
        // make a copy of clique
        TRACE("Saving solution", TRACE_MASK_CLIQUE, 2);
        TRACEVAR(c.size(), TRACE_MASK_CLIQUE, 2);
        {
            std::lock_guard<std::mutex> lk(mutexQ); 
            if (maxSize < c.size()) {
                maxSize = c.size();
                maxClique = c;
                //std::cout << "new clique " << maxSize << "\n";
            }
            ret = maxSize;
        }
        
        return ret;
    }
};


#endif // header guard 
