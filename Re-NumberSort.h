#ifndef HEADER_F7D1CB85B2C4397
#define HEADER_F7D1CB85B2C4397


#include "MaximumCliqueBase.h"


template<class ColorSort>
struct ReNumberSort : ColorSort {
    using ColorSort::numberSort;
    typedef typename ColorSort::GraphType GraphType;
    typedef typename GraphType::VertexSet VertexSet;
    typedef typename ColorSort::NumberedSet NumberedSet;
    typedef typename GraphType::VertexId VertexId;
    
    using ColorSort::init;
    
    // take clique c and candidate vertex set p as input
    // return numbered vertex set np as output
    void numberSort(const VertexSet& c, VertexSet& p, NumberedSet& np) {
        size_t m = p.size();
        size_t numbers = 0;
            
        for (size_t i = 0; i < m; i++) {
            this->colorSet[i].clear();
            VertexId v = p[i];
            size_t k = 0;
            
            while (intersectionExists(v, this->colorSet[k])) 
                k++;
                
            this->colorSet[k].add(v);
            numbers = std::max(numbers, k+1);
        }
        
        np.resize(m);
        for (size_t k = 0, i = 0; k < numbers; k++) {
            for (size_t j = 0; j < this->colorSet[k].size(); j++) {
                VertexId v = this->colorSet[k][j];
                p[i] = v; 
                np[i++] = k+1;
            }
        }
    }
    
    void renumber(VertexId p, VertexId no, VertexId threshold) {
        auto& pAdjecent = this->graph->adjacencyMatrix[p];
        for (VertexId k1 = 0; k1 < threshold-1; ++k1) {
            // find vertex from neighbourhood of p, that is of the color k1
            auto& color1 = this->colorSet[k1];
            for (auto& q : color1) {
                if (pAdjecent[q]) {
                    for (VertexId k2 = k1+1; k2 < threshold; ++k2) {
                        if (intersectionExists(q, this->colorSet[k2])) {
                            
                        }
                    }
                    // only do this for one vertex of each color → break the loop
                    break;
                }
            }
        }
    }
};


#endif // header guard 
