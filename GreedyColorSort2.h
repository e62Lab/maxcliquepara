#ifndef GREEDYCOLORSORT2_H
#define GREEDYCOLORSORT2_H


#include "MaximumCliqueBase.h"


template<class Graph>
class GreedyColorSort2 {
public:
    typedef typename Graph::VertexSet VertexSet;
    typedef typename VertexSet::VertexId VertexId;
    typedef std::vector<VertexId> NumberedSet;
    typedef Graph GraphType;
    
protected:
    std::vector<VertexSet> colorSet;
    const Graph* graph;
    Timers* timers;
    
public:
    GreedyColorSort2() : graph(nullptr) {}
    
    void init(const GraphType* g, Timers& t) {
        graph = g;
        timers = &t;
        size_t n = graph->getNumVertices();
        colorSet.resize(n);
        for (auto cc : colorSet)
            cc.reserve(n);
    }
    
    bool notEmpty(const NumberedSet& ns) const {return ns.size() > 0;}
    
    VertexId topNumber(const NumberedSet& ns) const {return ns.back();}
    
    VertexId topVertex(const NumberedSet& ns, const VertexSet& vs) const {return vs.back();}
    
    void popTop(NumberedSet& ns, VertexSet& vs) {ns.pop_back(); vs.pop();}
    
    // take clique c and candidate vertex set p as input
    // return numbered vertex set np as output
    void numberSort(const VertexSet& c, VertexSet& p, NumberedSet& np, unsigned int maxSize = 0) {
        size_t m = p.size();
        size_t numbers = 1;
        
        for (size_t i = 0; i < m; i++) {
            colorSet[i].clear();
            VertexId v = p[i];
            
            size_t k = 0;
            while (intersectionExists(v, colorSet[k])) 
                k++;
                
            colorSet[k].add(v);
            numbers = std::max(numbers, k+1);
            
            while ((k+1 < numbers) && (colorSet[k].size() > colorSet[k+1].size())) {
                std::swap(colorSet[k], colorSet[k+1]);
                --k;
            }
        }
        
        np.resize(m);
        for (size_t k = 0, i = 0; k < numbers; k++) {
            for (size_t j = 0; j < colorSet[k].size(); j++) {
                VertexId v = colorSet[k][j];
                p[i] = v; 
                np[i++] = k+1;
            }
        }
    }

protected:    
    bool intersectionExists(VertexId p, const VertexSet& vertices) const {
        const VertexSet& neighbourhood = graph->adjacencyMatrix[p];
        size_t n = vertices.size();
        for (size_t i = 0; i < n; ++i) {
            if (neighbourhood[vertices[i]]==1)
                return true;
        }
        return false;
    }
};

#endif // GREEDYCOLORSORT2_H

