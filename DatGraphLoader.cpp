#include "DatGraphLoader.h"
#include <fstream>
#include <regex>
#include <iostream>
#include <cassert>


DatGraphLoader::DatGraphLoader() {
    
}

DatGraphLoader::~DatGraphLoader() {
    
}

std::istream& safeGetline(std::istream& is, std::string& t) {
    t.clear();

    // The characters in the stream are read one-by-one using a std::streambuf.
    // That is faster than reading them one-by-one using the std::istream.
    // Code that uses streambuf this way must be guarded by a sentry object.
    // The sentry object performs various tasks,
    // such as thread synchronization and updating the stream state.

    std::istream::sentry se(is, true);
    std::streambuf* sb = is.rdbuf();

    for(;;) {
        int c = sb->sbumpc();
        switch (c) {
        case '\n':
            return is;
        case '\r':
            if(sb->sgetc() == '\n')
                sb->sbumpc();
            return is;
        case std::streambuf::traits_type::eof():
            // Also handle the case when the last line has no line ending
            if(t.empty())
                is.setstate(std::ios::eofbit);
            return is;
        default:
            t += (char)c;
        }
    }
}

bool DatGraphLoader::load(const char* fname) {
    std::ifstream file(fname);
    error = "";

    if (!file) {
        error = "ifstream invalid - file cannot be opened for reading";
        return false;
    }
    
    int lineNumber = 0;
    std::string line;
    try {
        std::regex ws_re("\\s+");
        while (file && error.empty()) {
            safeGetline(file, line);
            
            // the following checks should never trigger on a correctly stored file
            if (line.empty())
                break;
                
            if (lineNumber < 1) {
                // read the number of vertices and edges
                std::vector<std::string> result { std::sregex_token_iterator(line.begin(), line.end(), ws_re, -1), {} };
                if (result.size() != 2) {
                    error = "First line of file should be formed as follows: 'n m', where n is the number of vertices and m the number of edges.";
                    return false;
                }
                numVertices = std::stoi(result[0]);
                numEdges = std::stoi(result[1]);
                // sanity check
                const int limit = 30000;
                if ((numVertices > limit) || (numVertices < 1)) {
                    error == "Number of vertices is too large ("+std::to_string(numVertices)+"), this library has a limit of "+std::to_string(limit);
                    return false;
                } else 
                    successorMatrix.resize(numVertices);
            } else { // lineNumber is 1 or more
                if (lineNumber > numEdges)
                    break;
                
                std::vector<std::string> result { std::sregex_token_iterator(line.begin(), line.end(), ws_re, -1), {} };
                if (result.size() != 3) {
                    error = "Line containing edge definition should consist of exactly 2 node indices, followed by floating point weight.";
                    return false;
                } else {
                    // parse line and store data 
                    int n1 = std::stoi(result[0])-1;
                    int n2 = std::stoi(result[1])-1;
                    // weights are discarded at this point
                    // float w = std::stof(result[2]);
                    insertToMatrix(n1, n2);
                    insertToMatrix(n2, n1);
                    
                }
            }
            ++lineNumber;
        }
        
        if (DEBUG_LOADER) {
            std::cout << "\n " << successorMatrix.size() << " vertices\n";
            if (successorMatrix.size() > 0)
            for (size_t i = 0; i < successorMatrix.size(); ++i) {
                std::cout << i << ": ";
                for (size_t  j = 0; j < successorMatrix[i].size(); ++j)
                    std::cout << successorMatrix[i][j] << " ";
                std::cout << ";\n";
            }
        }        
    } catch (std::invalid_argument& e) {
        error = "Invalid argument provided to stoi in line " + std::to_string(lineNumber)+ ": " + line +"\n";
    }
    
    if (lineNumber <= numVertices) {
        error = "The number of lines read is too small: " + std::to_string(lineNumber)+ "\n";
        return false;
    } else 
        return true;
}

unsigned int DatGraphLoader::getNumVertices() const {
    return numVertices;
}

unsigned int DatGraphLoader::getMaxVertiexIndex() const {
    return numVertices-1;
}

unsigned int DatGraphLoader::getNumEdges() const {
    unsigned int s = 0;
    for (size_t i = 0; i < successorMatrix.size(); ++i) {
        s += successorMatrix[i].size();
    }
    return s/2;
}

std::vector<std::vector<char>> DatGraphLoader::getAdjacencyMatrix() const {
    std::vector<std::vector<char>> adjacency(successorMatrix.size());
    for (size_t i = 0; i < successorMatrix.size(); ++i) {
        adjacency[i].resize(successorMatrix.size(), false);
        for (size_t j = 0; j < successorMatrix[i].size(); ++j) {
            size_t i2 = successorMatrix[i][j];
            adjacency[i][i2] = true;
        }
    }
    return adjacency;
}

std::vector<int> DatGraphLoader::getDegrees() const {
    std::vector<int> degrees(successorMatrix.size(), 0);
    for (size_t i = 0; i < successorMatrix.size(); ++i) {
        degrees[i] = successorMatrix[i].size();
    }
    return degrees;
}

void DatGraphLoader::insertToMatrix(unsigned int i1, unsigned int i2) {
    assert(i1 < successorMatrix.size());
    assert(i2 < successorMatrix.size());
    
    int insertPos = 0;
    for (size_t i = 0; i < successorMatrix[i1].size(); ++i) {
        if (successorMatrix[i1][i] < i2) {
            insertPos = i+1;
        } else if (successorMatrix[i1][i] == i2) {
            insertPos = -1;
            break;
        } else
            break;
    }
    if (insertPos != -1)
        successorMatrix[i1].insert(successorMatrix[i1].begin()+insertPos, i2);
}

