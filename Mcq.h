#ifndef MCQ_H
#define MCQ_H


#include "MaximumCliqueBase.h"


struct ExtendedVertex {
    int index;
    int degree;
    int color;
    int saturation;
    int nebDeg;
    std::vector<bool> domain;
    
    void init(int i, int d) {
        index = i;
        degree = d;
        nebDeg = 0;
    }
    
    void init(int n){
        domain.resize(n, true); 
        color = -1;
        saturation = 0;
    }

    // color vertex with lowest possible color
    int colorize() {
        for (color = 0; !domain[color]; color++);
        domain[color] = false;
        return color;
    }

    void removeColour(int i) {
        if (domain[i]) {
            saturation++; 
            domain[i] = false;
        }
    }

    bool operator< (const ExtendedVertex& v) const {
        return ((degree > v.degree) || ((degree == v.degree) && (index < v.index)));
    }
    
    bool operator== (const ExtendedVertex& v) const {
        return index == v.index;
    }
};


template<class Ostream>
Ostream& operator<< (Ostream& out, const ExtendedVertex& ev) {
    return out << ev.index << "˙" << ev.color << "˙" << ev.nebDeg << " ";
}


class Mcq : public MaximumCliqueBase {
public:
    typedef std::vector<ExtendedVertex> EVertexList;
    
protected:
    VertexMatrix colorClass;
    int sortingStyle;
    
public:
    enum {
        sort_degree,
        sort_minWidth,
        sort_mcr,
        sort_invalid
    };
    
public:
    Mcq();

    Mcq(int n, const std::vector<std::vector<int> >& A, const std::vector<int>& degree, int sortingStyle = sort_degree);
    
    void setSortingStyle(int style) {sortingStyle = style;}
    void search();
    void expand(VertexList& C, VertexList& P);
    void numberSort(VertexList& ColOrd, VertexList& P, VertexList& color);
    bool conflicts(int v, VertexList& colors);
    void orderVertices(VertexList& ColOrd);
    void minWidthOrder(EVertexList& V);
   
};


#endif // MCQ_H
